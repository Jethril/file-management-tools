# File Management Tools library
_Manage your files in a clean, modern way_

## Features
- Operating system detection
- Endianness detection
- Cross-platform file system path manipulation
- Directory creation, move and deletion
- File manipulation with full encoding conversion support
- Symlink management
- High and low level stream-based API
- Safe file type recognition

Planned:
- [ ] Glob support
- [ ] Pure PHP archive management (rar, zip, tar...)
- [ ] Suggest!

## Getting started
You can find the project documentation and a small "getting started"
[here](https://gitlab.com/EclipseOnFire/file-management-tools/wikis/home).

## Examples
Some examples to give you a small overview.

Fully reading and writing files:
```php
try {
    // Simple static methods, no instantiation overhead.
    File::writeAllText('some/file.txt', 'Hello!');

    $text = File::readAllText('some/file.txt');
} catch (FileNotFoundException | FileNotReadableException | IOException $e) {
    // Clear exceptions, goodbye "return false" & warnings!
    return ['status' => 500, 'error' => 'Internal Server Error'];
}
```

Simple file and directory manipulation:
```php
// High-level object-oriented abstraction over native functions,
// never use unlink() or rename() again!
File::move('some/file.txt', 'file.txt');
File::setModificationDate('file.txt', new DateTime());
File::delete('some-file.txt');

Directory::copy('some/dir', 'other/dir');
Directory::createAll('these/folders/will/be/created');
```

Reading raw data from files:
```php
// Use real stream objects, like modern languages do.
$stream = File::openRead('some-file');

try {
    $bool = $stream->readBool();
    $int  = $stream->readInt();
    $uint = $stream->readUnsignedInt(Endianness::LITTLE);
    $row  = $stream->readCsv(';', '"', '\\', Encoding::WINDOWS_1252);
    
    foreach ($stream->lines() as $line) {
        // Iterate over lines lazily using generators!
    }
} finally {
    $stream->close();
}
```

Path manipulation:
```php
// Cross-platform path joining, handling trailing and leading separators.
$uploads = Path::join(__DIR__, '../../uploads');

// Easily make absolute and relative paths.
$uploads = Path::makeAbsolute($uploads, __DIR__);
$path    = Path::join($uploads, $_GET['file']);

// Safely embed variables in paths without risking directory traversals.
if (!Path::startsWith($path, $uploads)) {
    return ['status' => 400, 'error' => 'Invalid file name provided (I got you!)'];
}
```

