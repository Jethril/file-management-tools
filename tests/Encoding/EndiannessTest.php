<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\Tests\Encoding;

use FileManagementTools\Encoding\Endianness;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \FileManagementTools\Encoding\Endianness
 */
final class EndiannessTest extends TestCase
{
    public function testConvertShortSameEndianness(): void
    {
        static::assertSame(0x00FF, Endianness::convertShort(0x00FF, Endianness::LITTLE, Endianness::LITTLE));
    }

    public function testConvertShortLittleToBigEndianness(): void
    {
        static::assertSame(0xFF00, Endianness::convertShort(0x00FF, Endianness::LITTLE, Endianness::BIG));
    }

    public function testConvertShortBigToLittleEndianness(): void
    {
        static::assertSame(0x00FF, Endianness::convertShort(0xFF00, Endianness::BIG, Endianness::LITTLE));
    }

    public function testConvertIntegerSameEndianness(): void
    {
        static::assertSame(0xFF000000, Endianness::convertInt(0xFF000000, Endianness::LITTLE, Endianness::LITTLE));
    }

    public function testConvertIntegerLittleToBigEndianness(): void
    {
        static::assertSame(0x000000FF, Endianness::convertInt(0xFF000000, Endianness::LITTLE, Endianness::BIG));
    }

    public function testConvertIntegerBigToLittleEndianness(): void
    {
        static::assertSame(0xFF000000, Endianness::convertInt(0x000000FF, Endianness::BIG, Endianness::LITTLE));
    }
}
