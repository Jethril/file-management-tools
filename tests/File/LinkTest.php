<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\Tests\File;

use FileManagementTools\File\Directory;
use FileManagementTools\File\Link;
use FileManagementTools\File\Path;
use PHPUnit\Framework\TestCase;

/**
 * @group  links
 *
 * @internal
 * @covers \FileManagementTools\File\Link
 */
final class LinkTest extends TestCase
{
    private $dir;

    protected function setUp()
    {
        $this->dir = tempnam(sys_get_temp_dir(), 'tests');

        unlink($this->dir);
        mkdir($this->dir);

        touch(Path::join($this->dir, 'file1'));
        symlink(Path::join($this->dir, 'file1'), Path::join($this->dir, 'file1-link'));

        mkdir(Path::join($this->dir, 'folder1'));
        symlink(Path::join($this->dir, 'folder1'), Path::join($this->dir, 'folder1-link'));
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    protected function tearDown()
    {
        Directory::delete($this->dir);
    }

    public function testExists(): void
    {
        static::assertTrue(Link::exists(Path::join($this->dir, 'folder1-link')));
        static::assertFalse(Link::exists(Path::join($this->dir, 'folder1')));
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testCreate(): void
    {
        $path = Path::join($this->dir, 'some-link');

        Link::create($path, Path::join($this->dir, 'folder1'));

        static::assertDirectoryExists($path);
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testCreateWithRelativePath(): void
    {
        $path = Path::join($this->dir, 'some-link');

        Link::create($path, Path::join($this->dir, 'folder1'));

        static::assertDirectoryExists($path);
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testDelete(): void
    {
        Link::delete(Path::join($this->dir, 'file1-link'));

        static::assertFileExists(Path::join($this->dir, 'file1'));
        static::assertFileNotExists(Path::join($this->dir, 'file1-link'));

        Link::delete(Path::join($this->dir, 'folder1-link'));

        static::assertDirectoryExists(Path::join($this->dir, 'folder1'));
        static::assertDirectoryNotExists(Path::join($this->dir, 'folder1-link'));
    }

    /**
     * @expectedException \FileManagementTools\File\Exceptions\LinkNotFoundException
     *
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testDeleteNonExisting(): void
    {
        Link::delete('some-non-existing-link');
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testDeleteNonExistingIgnore(): void
    {
        Link::delete('some-non-existing-link', true);

        static::assertTrue(true);
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testGetTarget(): void
    {
        static::assertSame(Path::join($this->dir, 'folder1'), Link::getTarget(Path::join($this->dir, 'folder1-link')));
    }
}
