<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\Tests\File;

use FileManagementTools\File\Path;
use FileManagementTools\OperatingSystem\OperatingSystem;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \FileManagementTools\File\Path
 */
final class PathTest extends TestCase
{
    public function testIsRoot(): void
    {
        static::assertTrue(Path::isRoot('C:'));
        static::assertTrue(Path::isRoot('c:'));
        static::assertTrue(Path::isRoot('/'));
    }

    public function testIsRootWithTrailingSlashes(): void
    {
        static::assertTrue(Path::isRoot('C:\\\\'));
        static::assertTrue(Path::isRoot('c:\\\\'));
        static::assertTrue(Path::isRoot('C://'));
        static::assertTrue(Path::isRoot('c://'));
        static::assertTrue(Path::isRoot('///'));
    }

    public function testJoinPathWithoutTrailingSeparators(): void
    {
        static::assertSame('some/path', Path::joinWithOs(OperatingSystem::LINUX, 'some', 'path'));
        static::assertSame('some\\path', Path::joinWithOs(OperatingSystem::WINDOWS, 'some', 'path'));
    }

    public function testJoinPathWithTrailingSeparators(): void
    {
        static::assertSame('/some/path', Path::joinWithOs(OperatingSystem::LINUX, '/', 'some\\\\path/'));
        static::assertSame('/some/path', Path::joinWithOs(OperatingSystem::LINUX, '/some\\\\path/'));
        static::assertSame('some\\path', Path::joinWithOs(OperatingSystem::WINDOWS, 'some//path\\'));
        static::assertSame('\\some\\path', Path::joinWithOs(OperatingSystem::WINDOWS, '\\some//path\\'));
    }

    public function testJoinPathWithDots(): void
    {
        static::assertSame(
            '.././some/path/.',
            Path::joinWithOs(OperatingSystem::LINUX, '../.', 'some\\path', '.', '/')
        );

        static::assertSame(
            '..\\.\\some\\path\\.',
            Path::joinWithOs(OperatingSystem::WINDOWS, '..\\.', 'some/path', '.', '\\')
        );
    }

    public function testGetFileNameWithSingleFile(): void
    {
        static::assertSame('test.txt', Path::getFileName('test.txt'));
    }

    public function testGetFileNameWithSingleFileWithoutExtension(): void
    {
        static::assertSame('test', Path::getFileName('test'));
    }

    public function testGetFileNameWithPath(): void
    {
        static::assertSame('test.txt', Path::getFileName('/path/test.txt'));
        static::assertSame('test.txt', Path::getFileName('\\path\\test.txt'));
    }

    public function testGetFileNameWithTrailingSlashes(): void
    {
        static::assertSame('test.txt', Path::getFileName('.././path/./test.txt//'));
        static::assertSame('test.txt', Path::getFileName('..\\.\\path\\.\\test.txt\\\\'));
    }

    public function testGetFileNameWithWindowsRootPath(): void
    {
        static::assertNull(Path::getFileName('C:'));
        static::assertNull(Path::getFileName('C://'));
        static::assertNull(Path::getFileName('C:\\\\'));
    }

    public function testGetExtensionSingleFile(): void
    {
        static::assertSame('txt', Path::getExtension('test.txt'));
    }

    public function testGetExtensionSingleFileWithoutExtension(): void
    {
        static::assertNull(Path::getExtension('test'));
    }

    public function testGetExtensionUpperCase(): void
    {
        static::assertSame('txt', Path::getExtension('/path/test.TXT'));
        static::assertSame('txt', Path::getExtension('\\path\\test.TXT'));
    }

    public function testGetExtensionWithLeadingDot(): void
    {
        static::assertNull(Path::getExtension('/path/.git'));
        static::assertNull(Path::getExtension('\\path\\.git'));
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testGetExtensionWithTrailingDot(): void
    {
        Path::getExtension('/path/git.');
    }

    public function testGetExtensionTrailingSlashes(): void
    {
        static::assertSame('txt', Path::getExtension('.././path/./test.txt//'));
        static::assertSame('txt', Path::getExtension('..\\.\\path\\.\\test.txt\\\\'));
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testGetExtensionWithInvalidFileName(): void
    {
        Path::getExtension('../.\\..');
    }

    public function testGetExtensionWithRootPath(): void
    {
        static::assertNull(Path::getExtension('C:'));
        static::assertNull(Path::getExtension('c:'));
        static::assertNull(Path::getExtension('/'));
    }

    public function testGetFileNameWithoutExtensionSingleFile(): void
    {
        static::assertSame('test', Path::getFileNameWithoutExtension('test.txt'));
    }

    public function testGetFileNameWithoutExtensionSingleFileWithoutExtension(): void
    {
        static::assertSame('test', Path::getFileNameWithoutExtension('test'));
        static::assertSame('test', Path::getFileNameWithoutExtension('test'));
    }

    public function testGetFileNameWithoutExtensionUpperCase(): void
    {
        static::assertSame('test', Path::getFileNameWithoutExtension('/path/test.TXT'));
        static::assertSame('test', Path::getFileNameWithoutExtension('\\path\\test.TXT'));
    }

    public function testGetFileNameWithoutExtensionWithLeadingDot(): void
    {
        static::assertSame('.git', Path::getFileNameWithoutExtension('/path/.git'));
        static::assertSame('.git', Path::getFileNameWithoutExtension('\\path\\.git'));
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testGetFileNameWithoutExtensionWithTrailingDot(): void
    {
        Path::getFileNameWithoutExtension('/path/git.');
    }

    public function testGetFileNameWithoutExtensionTrailingSlashes(): void
    {
        static::assertSame('test', Path::getFileNameWithoutExtension('.././path/./test.txt//'));
        static::assertSame('test', Path::getFileNameWithoutExtension('..\\.\\path\\.\\test.txt\\\\'));
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testGetFileNameWithoutExtensionWithInvalidFileName(): void
    {
        Path::getFileNameWithoutExtension('../.\\..');
    }

    public function testGetFileNameWithoutExtensionWithRootPath(): void
    {
        static::assertNull(Path::getFileNameWithoutExtension('C:'));
        static::assertNull(Path::getFileNameWithoutExtension('c:'));
        static::assertNull(Path::getFileNameWithoutExtension('/'));
    }

    public function testGetParentSingleFile(): void
    {
        static::assertNull(Path::getParentSegment('test.txt'));
    }

    public function testGetParentWithRootPath(): void
    {
        static::assertSame('/', Path::getParentSegment('/file.txt'));

        static::assertSame('C:', Path::getParentSegment('C:\\file.txt'));
        static::assertSame('c:', Path::getParentSegment('c:/file.txt'));
        static::assertSame('\\', Path::getParentSegment('\\file.txt'));
    }

    public function testIsValidFileNameWithValidFileNames(): void
    {
        static::assertTrue(Path::isValidFileName('someFile.txt'));
        static::assertTrue(Path::isValidFileName('some-file1.txt'));
        static::assertTrue(Path::isValidFileName('some_file2.txt'));
        static::assertTrue(Path::isValidFileName('some file3.txt'));
        static::assertTrue(Path::isValidFileName('some file 6'));
        static::assertTrue(Path::isValidFileName('.some.file.txt'));
    }

    public function testIsValidFileNameWithInvalidValidFileNames(): void
    {
        static::assertFalse(Path::isValidFileName(''));
        static::assertFalse(Path::isValidFileName('AUX'));
        static::assertFalse(Path::isValidFileName('invalid\\file\\name'));
        static::assertFalse(Path::isValidFileName('invalid.file.'));
        static::assertFalse(Path::isValidFileName('.'));
        static::assertFalse(Path::isValidFileName('..'));
        static::assertFalse(Path::isValidFileName('...'));
    }

    public function testIsValidSegmentWithValidSegments(): void
    {
        static::assertTrue(Path::isValidSegment('someFile.txt'));
        static::assertTrue(Path::isValidSegment('some-file1.txt'));
        static::assertTrue(Path::isValidSegment('some_file2.txt'));
        static::assertTrue(Path::isValidSegment('some file3.txt'));
        static::assertTrue(Path::isValidSegment('some file 6'));
        static::assertTrue(Path::isValidSegment('.some.file.txt'));
        static::assertTrue(Path::isValidSegment('.'));
        static::assertTrue(Path::isValidSegment('..'));
    }

    public function testIsValidSegmentWithInvalidValidSegments(): void
    {
        static::assertFalse(Path::isValidSegment(''));
        static::assertFalse(Path::isValidSegment('AUX'));
        static::assertFalse(Path::isValidSegment('invalid\\file\\name'));
        static::assertFalse(Path::isValidSegment('invalid.file.'));
    }

    public function testRemoveTrailingSeparators(): void
    {
        static::assertSame('/test', Path::removeTrailingSeparators('/test'));
        static::assertSame('/test', Path::removeTrailingSeparators('/test\\\\'));
        static::assertSame('\\\\test', Path::removeTrailingSeparators('\\\\test/'));
    }

    public function testRemoveLeadingSeparators(): void
    {
        static::assertSame('test/', Path::removeLeadingSeparators('test/'));
        static::assertSame('test\\\\', Path::removeLeadingSeparators('/test\\\\'));
        static::assertSame('test/', Path::removeLeadingSeparators('\\\\test/'));
    }

    public function testIsValidPathWithValidPaths(): void
    {
        static::assertTrue(Path::isValidPath('/test/é/test.txt'));
        static::assertTrue(Path::isValidPath('/test\\test.txt'));
        static::assertTrue(Path::isValidPath('.\\test\\.\\..\\test.txt'));
        static::assertTrue(Path::isValidPath('/test///./test.txt'));
    }

    public function testIsValidPathWithInvalidPaths(): void
    {
        static::assertFalse(Path::isValidPath('some/CON/path'));
        static::assertFalse(Path::isValidPath('F:::/file'));
        static::assertFalse(Path::isValidPath('some/*/file.txt'));
    }

    public function testIsAbsolutePathWithAbsolutePaths(): void
    {
        static::assertTrue(Path::isAbsolute('//test/é/test.txt'));
        static::assertTrue(Path::isAbsolute('F:/test.txt'));
        static::assertTrue(Path::isAbsolute('F:\\users\\kevin\\desktop'));
        static::assertTrue(Path::isAbsolute('F:'));
        static::assertTrue(Path::isAbsolute('/'));
    }

    public function testIsAbsolutePathWithRelativePaths(): void
    {
        static::assertFalse(Path::isAbsolute('./test.txt'));
        static::assertFalse(Path::isAbsolute('../some-file.txt'));
        static::assertFalse(Path::isAbsolute('..\\relative-path.txt'));
        static::assertFalse(Path::isAbsolute('some-file.txt'));
    }

    public function testMakeAbsolute(): void
    {
        static::assertSame('/home/smith', Path::makeAbsolute('smith', '/home', OperatingSystem::LINUX));
        static::assertSame('/some/dir', Path::makeAbsolute('../some/./dir', '/home', OperatingSystem::LINUX));
        static::assertSame('/home', Path::makeAbsolute('/home', '/home', OperatingSystem::LINUX));

        static::assertSame('C:\\Users', Path::makeAbsolute('Users', 'C:', OperatingSystem::WINDOWS));
        static::assertSame('C:\\Users', Path::makeAbsolute('Users', 'C:', OperatingSystem::WINDOWS));
        static::assertSame(
            'C:\\some\\dir',
            Path::makeAbsolute('..\\some\\.\\dir', 'C:\\Users', OperatingSystem::WINDOWS)
        );
        static::assertSame('C:\\Windows', Path::makeAbsolute('\\Windows', 'C:\\Users', OperatingSystem::WINDOWS));
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testMakeAbsoluteWithRelativeCurrentPath(): void
    {
        Path::makeAbsolute('test.txt', '../parent');
    }

    public function testMakeRelative(): void
    {
        static::assertSame('users', Path::makeRelative('/home/users', '/home', OperatingSystem::LINUX));
        static::assertSame('.', Path::makeRelative('/', '/', OperatingSystem::LINUX));
        static::assertSame('..', Path::makeRelative('/', '/home', OperatingSystem::LINUX));
        static::assertSame('../../users', Path::makeRelative('/home/users', '/home/other/dir', OperatingSystem::LINUX));
        static::assertSame('..', Path::makeRelative('./../users/..', '/home/other/dir', OperatingSystem::LINUX));

        static::assertSame('Users', Path::makeRelative('C:\\Users', 'C:', OperatingSystem::WINDOWS));
        static::assertSame('.', Path::makeRelative('C:', 'C:', OperatingSystem::WINDOWS));
        static::assertSame('..', Path::makeRelative('C:', 'C:\\Users', OperatingSystem::WINDOWS));
        static::assertSame(
            '..\\..\\users',
            Path::makeRelative('F:\\home\\users', 'F:\\home\\other\\dir', OperatingSystem::WINDOWS)
        );
        static::assertSame(
            '..',
            Path::makeRelative('.\\..\\users\\..', 'F:\\home\\other\\dir', OperatingSystem::WINDOWS)
        );
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testMakeRelativeWithRelativeCurrentPath(): void
    {
        Path::makeRelative('C:\\users\\james\\desktop\\test.txt', '../parent');
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testMakeRelativeWithDifferentRoots(): void
    {
        Path::makeRelative('C:\\users\\james\\desktop\\test.txt', 'F:\\parent');
    }

    public function testAreEqualWithEqualPaths(): void
    {
        static::assertTrue(Path::areEqual('/home/jon/snow', '/home/jon/snow', OperatingSystem::LINUX));
        static::assertTrue(Path::areEqual('/home/jon//snow/', '/home/jon/snow', OperatingSystem::LINUX));
        static::assertTrue(Path::areEqual('/home/jon snow/', '/home/jon snow', OperatingSystem::LINUX));

        static::assertTrue(Path::areEqual('C:\\users\\jon\\snow', 'c:\\Users\\Jon\\snow', OperatingSystem::WINDOWS));
        static::assertTrue(Path::areEqual('C:\\users\\\\jon snow\\', 'c:\\users\\jon snow', OperatingSystem::WINDOWS));
        static::assertTrue(Path::areEqual('C:\\users\\\\jon snow\\', 'c:\\users\\jon snow', OperatingSystem::WINDOWS));
    }

    public function testAreEqualWithDifferentPaths(): void
    {
        static::assertFalse(Path::areEqual('/home/snow/', 'snow', OperatingSystem::LINUX));
        static::assertFalse(Path::areEqual('/snow', 'snow', OperatingSystem::LINUX));
        static::assertFalse(Path::areEqual('/home/Jon/snow', '/home/jon/snow', OperatingSystem::LINUX));

        static::assertFalse(Path::areEqual('C:\\users\\jon\\snow', 'snow', OperatingSystem::WINDOWS));
        static::assertFalse(Path::areEqual('C:\\snow', '\\snow', OperatingSystem::WINDOWS));
        static::assertFalse(Path::areEqual('.\\snow\\', '\\.\\snow', OperatingSystem::WINDOWS));
    }

    public function testStartsWith(): void
    {
        static::assertTrue(Path::startsWith('./test', '.', OperatingSystem::LINUX));
        static::assertTrue(Path::startsWith('/home', '/', OperatingSystem::LINUX));
        static::assertTrue(Path::startsWith('/home', '/home/', OperatingSystem::LINUX));
        static::assertTrue(Path::startsWith('/home/jon', '/home', OperatingSystem::LINUX));
        static::assertTrue(Path::startsWith('./../users', './../', OperatingSystem::LINUX));

        static::assertTrue(Path::startsWith('.\\test', '.', OperatingSystem::WINDOWS));
        static::assertTrue(Path::startsWith('C:\\Users', 'C:', OperatingSystem::WINDOWS));
        static::assertTrue(Path::startsWith('C:\\Users', 'C:\\Users\\', OperatingSystem::WINDOWS));
        static::assertTrue(Path::startsWith('C:\\Users\\jon', 'c:\\users', OperatingSystem::WINDOWS));
        static::assertTrue(Path::startsWith('.\\..\\users', '.\\..\\', OperatingSystem::WINDOWS));
    }
}
