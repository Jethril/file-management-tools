<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\Tests\TypeSniffer;

use FileManagementTools\File\File;
use FileManagementTools\File\Path;
use FileManagementTools\Stream\StreamInterface;
use FileManagementTools\TypeSniffer\FileType;
use FileManagementTools\TypeSniffer\MicrosoftOfficeTypeSniffer;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \FileManagementTools\TypeSniffer\MicrosoftOfficeTypeSniffer
 */
final class MicrosoftOfficeTypeSnifferTest extends TestCase
{
    /**
     * @var string base path of test files
     */
    private $files;

    protected function setUp(): void
    {
        $this->files = Path::join(__DIR__, 'Files');
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingDocFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.doc'), static function (StreamInterface $s): void {
            $type = (new MicrosoftOfficeTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::DOC, $type->getCode());
            static::assertSame(FileType::FAMILY_DOCUMENT, $type->getFamily());
        });
    }

    public function testSniffingDocFileByFileName(): void
    {
        $type = (new MicrosoftOfficeTypeSniffer())->sniffName(Path::join($this->files, 'test.doc'));

        static::assertNotNull($type);
        static::assertSame(FileType::DOC, $type->getCode());
        static::assertSame(FileType::FAMILY_DOCUMENT, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingXlsFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.xls'), static function (StreamInterface $s): void {
            $type = (new MicrosoftOfficeTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::XLS, $type->getCode());
            static::assertSame(FileType::FAMILY_DOCUMENT, $type->getFamily());
        });
    }

    public function testSniffingXlsFileByFileName(): void
    {
        $type = (new MicrosoftOfficeTypeSniffer())->sniffName(Path::join($this->files, 'test.xls'));

        static::assertNotNull($type);
        static::assertSame(FileType::XLS, $type->getCode());
        static::assertSame(FileType::FAMILY_DOCUMENT, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingPptFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.ppt'), static function (StreamInterface $s): void {
            $type = (new MicrosoftOfficeTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::PPT, $type->getCode());
            static::assertSame(FileType::FAMILY_DOCUMENT, $type->getFamily());
        });
    }

    public function testSniffingPptFileByFileName(): void
    {
        $type = (new MicrosoftOfficeTypeSniffer())->sniffName(Path::join($this->files, 'test.ppt'));

        static::assertNotNull($type);
        static::assertSame(FileType::PPT, $type->getCode());
        static::assertSame(FileType::FAMILY_DOCUMENT, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingDocxFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.docx'), static function (StreamInterface $s): void {
            $type = (new MicrosoftOfficeTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::DOCX, $type->getCode());
            static::assertSame(FileType::FAMILY_DOCUMENT, $type->getFamily());
        });
    }

    public function testSniffingDocxFileByFileName(): void
    {
        $type = (new MicrosoftOfficeTypeSniffer())->sniffName(Path::join($this->files, 'test.docx'));

        static::assertNotNull($type);
        static::assertSame(FileType::DOCX, $type->getCode());
        static::assertSame(FileType::FAMILY_DOCUMENT, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingXlsxFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.xlsx'), static function (StreamInterface $s): void {
            $type = (new MicrosoftOfficeTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::XLSX, $type->getCode());
            static::assertSame(FileType::FAMILY_DOCUMENT, $type->getFamily());
        });
    }

    public function testSniffingXlsxFileByFileName(): void
    {
        $type = (new MicrosoftOfficeTypeSniffer())->sniffName(Path::join($this->files, 'test.xlsx'));

        static::assertNotNull($type);
        static::assertSame(FileType::XLSX, $type->getCode());
        static::assertSame(FileType::FAMILY_DOCUMENT, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingPptxFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.pptx'), static function (StreamInterface $s): void {
            $type = (new MicrosoftOfficeTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::PPTX, $type->getCode());
            static::assertSame(FileType::FAMILY_DOCUMENT, $type->getFamily());
        });
    }

    public function testSniffingPptxFileByFileName(): void
    {
        $type = (new MicrosoftOfficeTypeSniffer())->sniffName(Path::join($this->files, 'test.pptx'));

        static::assertNotNull($type);
        static::assertSame(FileType::PPTX, $type->getCode());
        static::assertSame(FileType::FAMILY_DOCUMENT, $type->getFamily());
    }
}
