<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\Tests\Stream;

use FileManagementTools\Encoding\Encoding;
use FileManagementTools\Encoding\Endianness;
use FileManagementTools\File\Exceptions\IOException;
use FileManagementTools\Stream\SeekPosition;
use FileManagementTools\Stream\StreamFactory;
use FileManagementTools\Stream\StreamInterface;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \FileManagementTools\Stream\MemoryStream
 * @covers \FileManagementTools\Stream\ResourceStream
 */
final class MemoryStreamTest extends TestCase
{
    /**
     * @expectedException \FileManagementTools\Exceptions\IllegalStateException
     */
    public function testUnclosedStreamThrowsException(): void
    {
        StreamFactory::createEmptyStream();
    }

    /**
     * @throws IOException
     */
    public function testUse(): void
    {
        $stream = StreamFactory::createEmptyStream();
        $result = $stream->use(static function (StreamInterface $s): int {
            return $s->writeBytes('b');
        });

        static::assertSame(1, $result);
        static::assertTrue($stream->isClosed());
    }

    /**
     * @throws IOException
     */
    public function testReadWriteCsvDefaultEncoding(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->writeCsv(['é', 'à', 'ç'], ';', '"', '\\');

            static::assertSame(['é', 'à', 'ç'], $s->seek(0)->readCsv(';', '"', '\\'));
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testReadWriteCsvWindows1252Encoding(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->writeCsv(['é', 'à', 'ç'], ';', '"', '\\', Encoding::WINDOWS_1252);

            static::assertSame(["\xe9", "\xe0", "\xe7"], $s->seek(0)->readCsv(';', '"', '\\'));

            static::assertSame(['é', 'à', 'ç'], $s->seek(0)->readCsv(';', '"', '\\', Encoding::WINDOWS_1252));
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testWriteStringDefaultEncoding(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->writeString('é à è ç');

            static::assertSame('é à è ç', $s->readAllBytes());
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testWriteStringWindows1252Encoding(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->writeString('é à ç', Encoding::WINDOWS_1252);

            static::assertSame("\xe9 \xe0 \xe7", $s->readAllBytes());
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testReadWriteLineDefaultEncoding(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->writeLine('é à ç');

            static::assertSame('é à ç', $s->seek(0)->readLine());
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testReadWriteLineWindows1252Encoding(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->writeLine('é à ç', Encoding::WINDOWS_1252);

            static::assertSame("\xe9 \xe0 \xe7", $s->seek(0)->readLine());
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testReadWriteLinesDefaultEncoding(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->writeLines(['é', 'à', 'ç']);

            static::assertSame(['é', 'à', 'ç'], iterator_to_array($s->seek(0)->lines()));
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testReadWriteLinesWindows1252Encoding(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->writeLines(['é', 'à', 'ç'], Encoding::WINDOWS_1252);

            static::assertSame(["\xe9", "\xe0", "\xe7"], iterator_to_array($s->seek(0)->lines()));
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testReadWriteByte(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->writeByte(125);

            static::assertSame(125, $s->seek(0)->readByte());
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testReadWriteBool(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->writeBool(true);
            $s->writeBool(false);

            static::assertTrue($s->seek(0)->readBool());
            static::assertFalse($s->readBool());
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testReadWriteUnsignedByte(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->writeUnsignedByte(255);

            static::assertSame(255, $s->seek(0)->readUnsignedByte());
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testReadWriteShort(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->writeShort(489, Endianness::LITTLE);

            static::assertSame(489, $s->seek(0)->readShort(Endianness::LITTLE));
            static::assertSame(-5887, $s->seek(0)->readShort(Endianness::BIG));
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testReadWriteUnsignedShort(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->writeUnsignedShort(489, Endianness::LITTLE);

            static::assertSame(489, $s->seek(0)->readUnsignedShort(Endianness::LITTLE));
            static::assertSame(59649, $s->seek(0)->readUnsignedShort(Endianness::BIG));
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testReadWriteInt(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->writeInt(489, Endianness::LITTLE);

            static::assertSame(489, $s->seek(0)->readInt(Endianness::LITTLE));
            static::assertSame(3909156864, $s->seek(0)->readInt(Endianness::BIG));
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testReadUnsignedInt(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->writeUnsignedInt(489, Endianness::LITTLE);

            static::assertSame(489, $s->seek(0)->readUnsignedInt(Endianness::LITTLE));
            static::assertSame(3909156864, $s->seek(0)->readUnsignedInt(Endianness::BIG));
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testReadWriteBytes(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->writeBytes("\xe9\xe0\xe7");

            static::assertSame("\xe9\xe0\xe7", $s->seek(0)->readBytes(3));
            static::assertSame("\xe9\xe0\xe7", $s->readAllBytes());
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testWriteBytesFromCurrentPosition(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->writeBytes("\xe9\xe0\xe7");

            static::assertSame("\xe9\xe0", $s->seek(0)->readBytes(2));
            static::assertSame("\xe7", $s->readAllBytes(false));
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testCopyToStream(): void
    {
        try {
            $source      = StreamFactory::createEmptyStream();
            $destination = StreamFactory::createEmptyStream();

            $source->writeBytes("\xe9\xe0\xe7");

            static::assertSame(3, $source->seek(0)->copyTo($destination));
            static::assertSame("\xe9\xe0\xe7", $destination->readAllBytes());
        } finally {
            if (isset($source)) {
                $source->close();
            }

            if (isset($destination)) {
                $destination->close();
            }
        }
    }

    /**
     * @throws IOException
     */
    public function testCopyToHandle(): void
    {
        try {
            $source      = StreamFactory::createEmptyStream();
            $destination = fopen('php://memory', 'rwb');

            $source->writeBytes("\xe9\xe0\xe7");

            static::assertSame(3, $source->seek(0)->copyTo($destination));

            fseek($destination, 0);

            static::assertSame("\xe9\xe0\xe7", stream_get_contents($destination));
        } finally {
            if (isset($source)) {
                $source->close();
            }

            if (isset($destination)) {
                fclose($destination);
            }
        }
    }

    /**
     * @throws IOException
     */
    public function testSeekStart(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->writeString('some string');

            static::assertSame('some string', $s->seek(0)->readBytes(11));
            static::assertSame('string', $s->seek(5)->readBytes(6));
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testSeekCursor(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->writeString('some string');
            $s->seek(-11, SeekPosition::CURSOR);

            static::assertSame('some string', $s->readBytes(11));

            $s->seek(-11, SeekPosition::CURSOR);
            $s->seek(5, SeekPosition::CURSOR);

            static::assertSame('string', $s->readBytes(6));
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testSeekEnd(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->writeString('some string');

            static::assertSame('some string', $s->seek(11, SeekPosition::END)->readBytes(11));
            static::assertSame('string', $s->seek(6, SeekPosition::END)->readBytes(6));
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testSize(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            static::assertSame(0, $s->size());

            $s->writeBool(false);

            static::assertSame(1, $s->seek(0)->size());
            static::assertSame(0, $s->tell());
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testAsPsrStream(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $stream = $s->asPsrStream();

            try {
                static::assertSame(11, $stream->write('some string'));
                static::assertSame('some string', (string) $stream);
            } finally {
                $stream->close();
            }
        })
        ;
    }

    /**
     * @expectedException \FileManagementTools\Exceptions\IllegalStateException
     * @throws IOException
     */
    public function testAsPsrStreamClosesSourceStream(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->asPsrStream();
            $s->readByte();
        })
        ;
    }

    /**
     * @throws IOException
     */
    public function testIsEOF(): void
    {
        StreamFactory::createEmptyStream()->use(static function (StreamInterface $s): void {
            $s->writeString('s');

            static::assertFalse($s->seek(0)->isEOF());
            static::assertSame(115, $s->readByte());
            static::assertTrue($s->isEOF());
        })
        ;
    }
}
