<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\Stream;

/**
 * Factory creating streams.
 */
final class StreamFactory
{
    private function __construct()
    {
    }

    /**
     * Creates a new stream from the provided resource.
     *
     * @param resource $resource a resource handle
     *
     * @return StreamInterface the created stream
     */
    public static function createStreamFromResource($resource): StreamInterface
    {
        return new ResourceStream($resource);
    }

    /**
     * Creates a new empty stream.
     *
     * @param bool $readable `true` if the stream should be readable, `false` otherwise
     * @param bool $writable `true` if the stream should be writable, `false` otherwise
     *
     * @return StreamInterface the created stream
     */
    public static function createEmptyStream(bool $readable = true, bool $writable = true): StreamInterface
    {
        return new MemoryStream($readable, $writable, null);
    }

    /**
     * Creates a new stream from the provided data.
     *
     * @param string $bytes    the data
     * @param bool   $readable `true` if the stream should be readable, `false` otherwise
     * @param bool   $writable `true` if the stream should be writable, `false` otherwise
     *
     * @return StreamInterface the created stream
     */
    public static function createStreamFromBytes(string $bytes, bool $readable, bool $writable): StreamInterface
    {
        return new MemoryStream($readable, $writable, $bytes);
    }
}
