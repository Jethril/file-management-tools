<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\Stream;

use FileManagementTools\Encoding\Encoding;
use FileManagementTools\Exceptions\IllegalStateException;
use FileManagementTools\File\Exceptions\IOException;
use Psr\Http\Message\StreamInterface as PsrStreamInterface;

/**
 * Interface used to represent data streams.
 */
interface StreamInterface
{
    /**
     * Executes the provided callable with this stream as argument, then flushes and closes the stream at the end of it.
     *
     * @param callable $callable a callable taking this stream as argument
     *
     * @throws IOException
     *
     * @return mixed what the callable returns
     */
    public function use(callable $callable);

    /**
     * Writes a CSV line to the stream.
     *
     * @param string[] $line      the line to write
     * @param string   $delimiter the CSV column delimiter
     * @param string   $enclosure the CSV cell enclosure
     * @param string   $escape    the CSV escape char
     * @param string   $encoding  the CSV file's encoding
     *
     * @throws IOException
     *
     * @return int the number of bytes written
     */
    public function writeCsv(
        array $line,
        string $delimiter,
        string $enclosure,
        string $escape,
        string $encoding = Encoding::UTF8
    ): int;

    /**
     * Writes a string to the stream.
     *
     * @param string $string   the string to write
     * @param string $encoding the target encoding
     *
     * @throws IOException
     *
     * @return int the number of bytes written
     */
    public function writeString(string $string, string $encoding = Encoding::UTF8): int;

    /**
     * Writes a single line to the stream.
     *
     * @param string $line     the line to write
     * @param string $encoding the target encoding
     *
     * @throws IOException
     *
     * @return int the number of bytes written
     */
    public function writeLine(string $line, string $encoding = Encoding::UTF8): int;

    /**
     * Writes several lines to the stream.
     *
     * @param iterable|string[] $lines    the lines to write
     * @param string            $encoding the target encoding
     *
     * @throws IOException
     *
     * @return int the number of bytes written
     */
    public function writeLines(iterable $lines, string $encoding = Encoding::UTF8): int;

    /**
     * Writes a byte to the stream.
     *
     * @param int $byte the byte to write
     *
     * @throws IOException
     */
    public function writeByte(int $byte): void;

    /**
     * Writes a boolean.
     *
     * @param bool $bool the boolean to write
     *
     * @throws IOException
     */
    public function writeBool(bool $bool): void;

    /**
     * Writes an unsigned byte to the stream.
     *
     * @param int $byte the byte to write
     *
     * @throws IOException
     */
    public function writeUnsignedByte(int $byte): void;

    /**
     * Writes a 16-bit integer.
     *
     * @param int      $short      the integer to write
     * @param null|int $endianness whether to use little or big endian. `null` means that it should be detected
     *
     * @throws IOException
     */
    public function writeShort(int $short, ?int $endianness = null): void;

    /**
     * Writes a 16-bit unsigned integer.
     *
     * @param int      $short      the integer to write
     * @param null|int $endianness whether to use little or big endian. `null` means that it should be detected
     *
     * @throws IOException
     */
    public function writeUnsignedShort(int $short, ?int $endianness = null): void;

    /**
     * Writes a 32-bit integer.
     *
     * @param int      $integer    the integer to write
     * @param null|int $endianness whether to use little or big endian. `null` means that it should be detected
     *
     * @throws IOException
     */
    public function writeInt(int $integer, ?int $endianness = null): void;

    /**
     * Writes an unsigned 32-bit integer.
     *
     * @param int      $integer    the integer to write
     * @param null|int $endianness whether to use little or big endian. `null` means that it should be detected
     *
     * @throws IOException
     */
    public function writeUnsignedInt(int $integer, ?int $endianness = null): void;

    /**
     * Writes a float number.
     *
     * @param float    $float      the float to write
     * @param null|int $endianness whether to use little or big endian. `null` means that it should be detected
     *
     * @throws IOException
     */
    public function writeFloat(float $float, ?int $endianness = null): void;

    /**
     * Writes a double number.
     *
     * @param float    $double     the double to write
     * @param null|int $endianness whether to use little or big endian. `null` means that it should be detected
     *
     * @throws IOException
     */
    public function writeDouble(float $double, ?int $endianness = null): void;

    /**
     * Writes several bytes to stream.
     *
     * @param string $bytes the bytes to write
     *
     * @throws IOException
     *
     * @return int the number of bytes written
     */
    public function writeBytes(string $bytes): int;

    /**
     * @param bool $fromBeginning whether to start from the beginning of the stream, or read the bytes from the current
     *                            position
     *
     * @throws IOException
     * @throws IllegalStateException if the stream is not seekable and the $fromBeginning parameter has been set to true
     *
     * @return string a binary string containing the full file
     */
    public function readAllBytes(bool $fromBeginning = true): string;

    /**
     * Reads a CSV line from the stream.
     *
     * @param string $delimiter the CSV column delimiter
     * @param string $enclosure the CSV cell enclosure
     * @param string $escape    the CSV escape char
     * @param string $encoding  the CSV file's encoding
     *
     * @throws IOException
     *
     * @return null|string[] the CSV line or null if the end of the stream has been reached
     */
    public function readCsv(
        string $delimiter,
        string $enclosure,
        string $escape,
        string $encoding = Encoding::UTF8
    ): ?array;

    /**
     * Reads a single byte from the stream.
     *
     * @throws IOException
     *
     * @return int the byte that was read or `null` if the end of the stream was reached
     */
    public function readByte(): ?int;

    /**
     * Reads a boolean.
     *
     * @throws IOException
     *
     * @return null|bool the boolean that was read, or `null` if the end of the stream was reached
     */
    public function readBool(): ?bool;

    /**
     * Reads an unsigned byte from the stream.
     *
     * @throws IOException
     *
     * @return int the byte that was read or `null` if the end of the stream was reached
     */
    public function readUnsignedByte(): ?int;

    /**
     * Reads a 16-bit integer.
     *
     * @param null|int $endianness whether to use little or big endian. `null` means that it should be detected
     *
     * @throws IOException
     *
     * @return null|int the integer that was read, or `null` if the end of the stream was reached
     */
    public function readShort(?int $endianness = null): ?int;

    /**
     * Reads a 16-bit unsigned integer.
     *
     * @param null|int $endianness whether to use little or big endian. `null` means that it should be detected
     *
     * @throws IOException
     *
     * @return null|int the integer that was read, or `null` if the end of the stream was reached
     */
    public function readUnsignedShort(?int $endianness = null): ?int;

    /**
     * Reads a 32-bit integer.
     *
     * @param null|int $endianness whether to use little or big endian. `null` means that it should be detected
     *
     * @throws IOException
     *
     * @return null|int the integer that was read, or `null` if the end of the stream was reached
     */
    public function readInt(?int $endianness = null): ?int;

    /**
     * Reads a 32-bit unsigned integer.
     *
     * **Note**: PHP will truncate the number if the number is greater than the max int value and the computer's
     * architecture is 32-bits.
     *
     * @param null|int $endianness whether to use little or big endian. `null` means that it should be detected
     *
     * @throws IOException
     *
     * @return null|int the integer that was read, or `null` if the end of the stream was reached
     */
    public function readUnsignedInt(?int $endianness = null): ?int;

    /**
     * Reads a float number.
     *
     * @param null|int $endianness whether to use little or big endian. `null` means that it should be detected
     *
     * @throws IOException
     *
     * @return null|float the float that was read, or `null` if the end of the stream was reached
     */
    public function readFloat(?int $endianness = null): ?float;

    /**
     * Reads a double number.
     *
     * @param null|int $endianness whether to use little or big endian. `null` means that it should be detected
     *
     * @throws IOException
     *
     * @return null|float the double that was read, or `null` if the end of the stream was reached
     */
    public function readDouble(?int $endianness = null): ?float;

    /**
     * Reads a number of bytes from the stream.
     *
     * @param int $length the number of bytes to read
     *
     * @throws IOException
     *
     * @return string the bytes read or null if the end of the stream was reached
     */
    public function readBytes(int $length): ?string;

    /**
     * Reads a single line from the stream.
     *
     * @param string $encoding the encoding
     *
     * @throws IOException
     *
     * @return null|string
     */
    public function readLine(string $encoding = Encoding::UTF8): ?string;

    /**
     * Lazily reads the lines until the end of the stream is reached.
     *
     * @param string $encoding the stream's encoding
     *
     * @throws IOException
     *
     * @return \Generator|string[] the stream's lines
     */
    public function lines(string $encoding = Encoding::UTF8): \Generator;

    /**
     * Copies this handle to another one.
     *
     * @param resource|StreamInterface $handle     another stream handle or a native resource
     * @param int                      $bufferSize the size of the buffer to use, in bytes
     *
     * @throws IOException
     *
     * @return int the number of bytes copied
     */
    public function copyTo($handle, int $bufferSize = 4096): int;

    /**
     * Moves the cursor to the provided offset.
     *
     * The offset in bytes must be positive and can only be negative when using {@see SeekPosition::CURSOR} position.
     *
     * @param int $offset   the offset in bytes
     * @param int $position the seek position (one of the {@see SeekPosition}::* constants)
     *
     * @throws IOException
     *
     * @return StreamInterface this object, for method chaining
     */
    public function seek(int $offset, int $position = SeekPosition::START): self;

    /**
     * @throws IOException
     *
     * @return null|int the stream's size in bytes, if available
     */
    public function size(): ?int;

    /**
     * @throws IOException
     *
     * @return int the stream's cursor position
     */
    public function tell(): int;

    /**
     * Flushes this stream.
     *
     * @throws IOException
     */
    public function flush(): void;

    /**
     * Closes this handle.
     *
     * @throws IOException
     */
    public function close(): void;

    /**
     * Creates a PSR stream from this stream.
     * After calling this method, this stream becomes unusable.
     *
     * @return PsrStreamInterface the created PSR stream
     */
    public function asPsrStream(): PsrStreamInterface;

    /**
     * Tests whether the end of the stream has been reached.
     *
     * @throws IOException
     *
     * @return bool true if the end of the stream is reached, false otherwise
     */
    public function isEOF(): bool;

    /**
     * @return bool true if the stream is seekable, false otherwise
     */
    public function isSeekable(): bool;

    /**
     * @return bool true if this handle is readable, false otherwise
     */
    public function isReadable(): bool;

    /**
     * @return bool true if this handle is writable, false otherwise
     */
    public function isWritable(): bool;

    /**
     * @return bool true if this handle is closed, false otherwise
     */
    public function isClosed(): bool;
}
