<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\Stream;

use FileManagementTools\File\Exceptions\IOException;
use Psr\Http\Message\StreamInterface as PsrStreamInterface;

/**
 * Class that wraps streams to provide a PSR stream support.
 *
 * @internal
 */
final class PsrResourceStreamAdapter implements PsrStreamInterface
{
    /**
     * @var ResourceStream the underlying stream
     */
    private $parent;

    /**
     * Constructs a new PSR stream adapter.
     *
     * @param ResourceStream $parent the underlying stream
     */
    public function __construct(ResourceStream $parent)
    {
        $this->parent = $parent;
    }

    /**
     * {@inheritdoc}
     */
    public function __toString(): string
    {
        try {
            return $this->parent->readAllBytes();
        } catch (\Throwable $e) {
            return '';
        }
    }

    /**
     * {@inheritdoc}
     */
    public function close(): void
    {
        try {
            $this->parent->close();
        } catch (\Throwable $t) {
        }
    }

    /**
     * {@inheritdoc}
     */
    public function detach()
    {
        return $this->parent->getResource();
    }

    /**
     * {@inheritdoc}
     */
    public function tell(): int
    {
        try {
            return $this->parent->tell();
        } catch (IOException $e) {
            throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function eof(): bool
    {
        try {
            return $this->parent->isEOF();
        } catch (IOException $e) {
            throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function seek($offset, $whence = SEEK_SET): void
    {
        try {
            switch ($whence) {
                case SEEK_SET:
                    $this->parent->seek((int) $offset);
                    break;
                case SEEK_CUR:
                    $this->parent->seek((int) $offset, SeekPosition::CURSOR);
                    break;
                case SEEK_END:
                    $this->parent->seek((int) $offset, SeekPosition::END);
                    break;
            }
        } catch (IOException $e) {
            throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rewind(): void
    {
        $this->seek(0);
    }

    /**
     * {@inheritdoc}
     */
    public function write($string): int
    {
        try {
            return $this->parent->writeBytes($string);
        } catch (IOException $e) {
            throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function read($length): string
    {
        try {
            return $this->parent->readBytes((int) $length);
        } catch (IOException $e) {
            throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getSize(): ?int
    {
        try {
            return $this->parent->size();
        } catch (IOException $e) {
            return null;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isSeekable(): bool
    {
        return $this->parent->isSeekable();
    }

    /**
     * {@inheritdoc}
     */
    public function isWritable(): bool
    {
        return $this->parent->isWritable();
    }

    /**
     * {@inheritdoc}
     */
    public function isReadable(): bool
    {
        return $this->parent->isWritable();
    }

    /**
     * {@inheritdoc}
     */
    public function getContents(): string
    {
        try {
            return $this->parent->readAllBytes(false);
        } catch (IOException $e) {
            throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMetadata($key = null)
    {
        $metadata = $this->parent->getStreamMetadata();

        return $key === null ? $metadata : $metadata[$key];
    }
}
