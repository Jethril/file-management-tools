<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\Stream;

use FileManagementTools\Encoding\Encoding;
use FileManagementTools\Encoding\Endianness;
use FileManagementTools\Exceptions\ArgumentOutOfRangeException;
use FileManagementTools\Exceptions\IllegalStateException;
use FileManagementTools\File\Exceptions\EndOfStreamException;
use FileManagementTools\File\Exceptions\IOException;

/**
 * A stream that wraps a PHP resource.
 */
class ResourceStream implements StreamInterface
{
    /**
     * @var null|resource the resource
     */
    private $resource;

    /**
     * @var bool determines if this stream is seekable
     */
    private $seekable;

    /**
     * @var bool determines if this stream is readable
     */
    private $readable;

    /**
     * @var bool determines if this stream is writable
     */
    private $writable;

    /**
     * @var bool determines if this stream has been closed
     */
    private $closed;

    /**
     * Constructs a new resource stream.
     *
     * @param null|resource $resource the resource
     */
    public function __construct($resource)
    {
        if (!\is_resource($resource)) {
            throw new \InvalidArgumentException('The provided resource is not valid!');
        }

        $metadata = stream_get_meta_data($resource);

        $this->resource = $resource;
        $this->readable = (bool) preg_match('/[r+]/i', $metadata['mode']);
        $this->writable = (bool) preg_match('/[waxc]/i', $metadata['mode']);
        $this->seekable = (bool) $metadata['seekable'];
        $this->closed   = (bool) $metadata['timed_out'];
    }

    public function __destruct()
    {
        if (!$this->closed) {
            throw new IllegalStateException('File stream has not been closed!');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function use(callable $callable)
    {
        if ($this->closed) {
            throw new IllegalStateException('This stream has been closed!');
        }

        try {
            return $callable($this);
        } finally {
            if (!$this->closed) {
                if ($this->writable) {
                    $this->flush();
                }

                $this->close();
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function writeCsv(
        array $line,
        string $delimiter,
        string $enclosure,
        string $escape,
        string $encoding = Encoding::UTF8
    ): int {
        if (!$this->readable) {
            throw new IllegalStateException('This stream is not readable!');
        }

        if ($this->closed) {
            throw new IllegalStateException('This stream has been closed!');
        }

        if ($encoding === Encoding::UTF8) {
            $written = @fputcsv($this->resource, $line, $delimiter, $enclosure, $escape);

            if ($written === false) {
                throw IOException::fromLastError();
            }

            return $written;
        }

        $tmp = @fopen('php://memory', 'wb+');

        if ($tmp === false) {
            throw IOException::fromLastError();
        }

        try {
            $written = @fputcsv($tmp, $line, $delimiter, $enclosure, $escape);

            if ($written === false) {
                throw IOException::fromLastError();
            }

            $result = @fseek($tmp, 0);

            if ($result === false || $result === -1) {
                throw IOException::fromLastError();
            }

            $result = @fread($tmp, $written);

            if ($result === false) {
                throw IOException::fromLastError();
            }

            return $this->writeString($result, $encoding);
        } finally {
            if (!@fclose($tmp)) {
                throw IOException::fromLastError();
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function writeString(string $string, string $encoding = Encoding::UTF8): int
    {
        if (!$this->writable) {
            throw new IllegalStateException('This stream is not writable!');
        }

        if ($this->closed) {
            throw new IllegalStateException('This stream has been closed!');
        }

        if ($encoding !== Encoding::UTF8) {
            $string = mb_convert_encoding($string, $encoding, Encoding::UTF8);
        }

        $written = @fwrite($this->resource, $string);

        if ($written === false) {
            throw IOException::fromLastError();
        }

        return $written;
    }

    /**
     * {@inheritdoc}
     */
    public function writeLine(string $line, string $encoding = Encoding::UTF8): int
    {
        return $this->writeString($line . PHP_EOL, $encoding);
    }

    /**
     * {@inheritdoc}
     */
    public function writeLines(iterable $lines, string $encoding = Encoding::UTF8): int
    {
        $total = 0;

        foreach ($lines as $line) {
            $total += $this->writeLine($line, $encoding);
        }

        return $total;
    }

    /**
     * {@inheritdoc}
     */
    public function writeByte(int $byte): void
    {
        if (!$this->writable) {
            throw new IllegalStateException('This stream is not writable!');
        }

        if ($this->closed) {
            throw new IllegalStateException('This stream has been closed!');
        }

        if ($byte < -128 || $byte > 127) {
            throw new \InvalidArgumentException('Byte value must be between -128 and 127 inclusive!');
        }

        if ((@fwrite($this->resource, pack('c', $byte))) === false) {
            throw IOException::fromLastError();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function writeBool(bool $bool): void
    {
        $this->writeByte((int) $bool);
    }

    /**
     * {@inheritdoc}
     */
    public function writeUnsignedByte(int $byte): void
    {
        if (!$this->writable) {
            throw new IllegalStateException('This stream is not writable!');
        }

        if ($this->closed) {
            throw new IllegalStateException('This stream has been closed!');
        }

        if ($byte < 0 || $byte > 255) {
            throw new \InvalidArgumentException('Byte value must be between 0 and 255 inclusive!');
        }

        if ((@fwrite($this->resource, pack('C', $byte))) === false) {
            throw IOException::fromLastError();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function writeShort(int $short, ?int $endianness = null): void
    {
        if ($short < -32768 || $short > 32767) {
            throw new \InvalidArgumentException('Short value must be between -32768 and 32767 inclusive!');
        }

        if ($endianness !== Endianness::BIG && $endianness !== Endianness::LITTLE) {
            throw new \InvalidArgumentException('Invalid endianness provided!');
        }

        if ($endianness !== null) {
            $short = Endianness::convertShort($short, Endianness::getCurrent(), $endianness);
        }

        $this->writeBytes(pack('s', $short));
    }

    /**
     * {@inheritdoc}
     */
    public function writeUnsignedShort(int $short, ?int $endianness = null): void
    {
        if ($short < 0 || $short > 65535) {
            throw new \InvalidArgumentException('Short value must be between 0 and 65535 inclusive!');
        }

        if ($endianness !== null && $endianness !== Endianness::BIG && $endianness !== Endianness::LITTLE) {
            throw new \InvalidArgumentException('Invalid endianness provided!');
        }

        if ($endianness !== null) {
            $short = Endianness::convertShort($short, Endianness::getCurrent(), $endianness);
        }

        $this->writeBytes(pack('S', $short));
    }

    /**
     * {@inheritdoc}
     */
    public function writeInt(int $integer, ?int $endianness = null): void
    {
        if ($integer < -2147483648 || $integer > 2147483647) {
            throw new \InvalidArgumentException('Integer value must be between -2147483648 and 2147483647 inclusive!');
        }

        if ($endianness !== Endianness::BIG && $endianness !== Endianness::LITTLE) {
            throw new \InvalidArgumentException('Invalid endianness provided!');
        }

        if ($endianness !== null) {
            $integer = Endianness::convertInt($integer, Endianness::getCurrent(), $endianness);
        }

        $this->writeBytes(pack('i', $integer));
    }

    /**
     * {@inheritdoc}
     */
    public function writeUnsignedInt(int $integer, ?int $endianness = null): void
    {
        if ($integer < 0 || $integer > 4294967295) {
            throw new \InvalidArgumentException('Integer value must be between 0 and 65535 inclusive!');
        }

        if ($endianness !== Endianness::BIG && $endianness !== Endianness::LITTLE) {
            throw new \InvalidArgumentException('Invalid endianness provided!');
        }

        $bytes = pack('I', $integer);

        if ($endianness !== null && $endianness !== Endianness::getCurrent()) {
            $bytes = strrev($bytes);
        }

        $this->writeBytes($bytes);
    }

    /**
     * {@inheritdoc}
     */
    public function writeFloat(float $float, ?int $endianness = null): void
    {
        switch ($endianness) {
            case null:
                $format = 'f';
                break;
            case Endianness::LITTLE:
                $format = 'g';
                break;
            case Endianness::BIG:
                $format = 'G';
                break;
            default:
                throw new \InvalidArgumentException('Invalid endianness provided!');
        }

        $this->writeBytes(pack($format, $float));
    }

    /**
     * {@inheritdoc}
     */
    public function writeDouble(float $double, ?int $endianness = null): void
    {
        switch ($endianness) {
            case null:
                $format = 'd';
                break;
            case Endianness::LITTLE:
                $format = 'e';
                break;
            case Endianness::BIG:
                $format = 'E';
                break;
            default:
                throw new \InvalidArgumentException('Invalid endianness provided!');
        }

        $this->writeBytes(pack($format, $double));
    }

    /**
     * {@inheritdoc}
     */
    public function writeBytes(string $bytes): int
    {
        if (!$this->writable) {
            throw new IllegalStateException('This stream is not writable!');
        }

        if ($this->closed) {
            throw new IllegalStateException('This stream has been closed!');
        }

        $written = @fwrite($this->resource, $bytes);

        if ($written === false) {
            throw IOException::fromLastError();
        }

        return $written;
    }

    /**
     * {@inheritdoc}
     */
    public function readAllBytes(bool $fromBeginning = true): string
    {
        if (!$this->readable) {
            throw new IllegalStateException('This stream is not readable!');
        }

        if ($this->closed) {
            throw new IllegalStateException('This stream has been closed!');
        }

        if ($fromBeginning && !$this->seekable) {
            throw new IllegalStateException('This stream is not seekable!');
        }

        $result = $fromBeginning
            ? @stream_get_contents($this->resource, -1, 0)
            : @stream_get_contents($this->resource, -1);

        if ($result === false) {
            throw IOException::fromLastError();
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function readCsv(
        string $delimiter,
        string $enclosure,
        string $escape,
        string $encoding = Encoding::UTF8
    ): ?array {
        if (!$this->readable) {
            throw new IllegalStateException('This stream is not readable!');
        }

        if ($this->closed) {
            throw new IllegalStateException('This stream has been closed!');
        }

        if ($this->isEOF()) {
            return null;
        }

        if ($encoding === Encoding::UTF8) {
            $result = @fgetcsv($this->resource, 0, $delimiter, $enclosure, $escape);

            if ($result === false) {
                throw IOException::fromLastError();
            }

            return $result;
        }

        $result = $this->readLine($encoding);

        return $result === null ? null : str_getcsv($result, $delimiter, $enclosure, $escape);
    }

    /**
     * {@inheritdoc}
     */
    public function readByte(): ?int
    {
        if (!$this->readable) {
            throw new IllegalStateException('Handle is not readable!');
        }

        if ($this->closed) {
            throw new IllegalStateException('Handle has been closed!');
        }

        if ($this->isEOF()) {
            return null;
        }

        $byte = @fread($this->resource, 1);

        if ($byte === false) {
            throw IOException::fromLastError();
        }

        return current(unpack('c', $byte));
    }

    /**
     * {@inheritdoc}
     */
    public function readUnsignedByte(): ?int
    {
        if (!$this->readable) {
            throw new IllegalStateException('Handle is not readable!');
        }

        if ($this->closed) {
            throw new IllegalStateException('Handle has been closed!');
        }

        if ($this->isEOF()) {
            return null;
        }

        $byte = @fread($this->resource, 1);

        if ($byte === false) {
            throw IOException::fromLastError();
        }

        return current(unpack('C', $byte));
    }

    /**
     * {@inheritdoc}
     */
    public function readBool(): ?bool
    {
        $byte = $this->readByte();

        return $byte === null ? null : $byte > 0;
    }

    /**
     * {@inheritdoc}
     */
    public function readShort(?int $endianness = null): ?int
    {
        if ($endianness !== null && $endianness !== Endianness::BIG && $endianness !== Endianness::LITTLE) {
            throw new \InvalidArgumentException('Invalid endianness provided!');
        }

        $bytes = $this->readBytes(2);

        if ($bytes === null) {
            return null;
        }

        if (\strlen($bytes) !== 2) {
            throw new EndOfStreamException();
        }

        if ($endianness !== null && $endianness !== Endianness::getCurrent()) {
            $bytes = strrev($bytes);
        }

        return current(unpack('s', $bytes));
    }

    /**
     * {@inheritdoc}
     */
    public function readUnsignedShort(?int $endianness = null): ?int
    {
        if ($endianness !== null && $endianness !== Endianness::BIG && $endianness !== Endianness::LITTLE) {
            throw new \InvalidArgumentException('Invalid endianness provided!');
        }

        $bytes = $this->readBytes(2);

        if ($bytes === null) {
            return null;
        }

        if (\strlen($bytes) !== 2) {
            throw new EndOfStreamException();
        }

        if ($endianness !== null && $endianness !== Endianness::getCurrent()) {
            $bytes = strrev($bytes);
        }

        return current(unpack('S', $bytes));
    }

    /**
     * {@inheritdoc}
     */
    public function readInt(?int $endianness = null): ?int
    {
        if ($endianness !== null && $endianness !== Endianness::BIG && $endianness !== Endianness::LITTLE) {
            throw new \InvalidArgumentException('Invalid endianness provided!');
        }

        $bytes = $this->readBytes(4);

        if ($bytes === null) {
            return null;
        }

        if (\strlen($bytes) !== 4) {
            throw new EndOfStreamException();
        }

        $value = current(unpack('i', $bytes));

        if ($endianness !== null) {
            $value = Endianness::convertInt($value, $endianness, Endianness::getCurrent());
        }

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function readUnsignedInt(?int $endianness = null): ?int
    {
        if ($endianness !== Endianness::BIG && $endianness !== Endianness::LITTLE) {
            throw new \InvalidArgumentException('Invalid endianness provided!');
        }

        $bytes = $this->readBytes(4);

        if ($bytes === null) {
            return null;
        }

        if (\strlen($bytes) !== 4) {
            throw new EndOfStreamException();
        }

        $value = current(unpack('I', $bytes));

        if ($endianness !== null) {
            $value = Endianness::convertInt($value, $endianness, Endianness::getCurrent());
        }

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function readFloat(?int $endianness = null): ?float
    {
        if ($endianness !== null && $endianness !== Endianness::BIG && $endianness !== Endianness::LITTLE) {
            throw new \InvalidArgumentException('Invalid endianness provided!');
        }

        $bytes = $this->readBytes(PHP_INT_SIZE); // Might not be accurate, but should work in most cases.

        if ($bytes === null) {
            return null;
        }

        if (\strlen($bytes) !== PHP_INT_SIZE) {
            throw new EndOfStreamException();
        }

        if ($endianness !== null && $endianness !== Endianness::getCurrent()) {
            $bytes = strrev($bytes);
        }

        return current(unpack('f', $bytes));
    }

    /**
     * {@inheritdoc}
     */
    public function readDouble(?int $endianness = null): ?float
    {
        if ($endianness !== null && $endianness !== Endianness::BIG && $endianness !== Endianness::LITTLE) {
            throw new \InvalidArgumentException('Invalid endianness provided!');
        }

        $bytes = $this->readBytes(PHP_INT_SIZE); // Might not be accurate, but should work in most cases.

        if ($bytes === null) {
            return null;
        }

        if (\strlen($bytes) !== PHP_INT_SIZE) {
            throw new EndOfStreamException();
        }

        if ($endianness !== null && $endianness !== Endianness::getCurrent()) {
            $bytes = strrev($bytes);
        }

        return current(unpack('d', $bytes));
    }

    /**
     * {@inheritdoc}
     */
    public function readBytes(int $length): ?string
    {
        if (!$this->readable) {
            throw new IllegalStateException('Handle is not readable!');
        }

        if ($this->closed) {
            throw new IllegalStateException('Handle has been closed!');
        }

        if ($this->isEOF()) {
            return null;
        }

        $result = @fread($this->resource, $length);

        if ($result === false) {
            throw IOException::fromLastError();
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function readLine(string $encoding = Encoding::UTF8): ?string
    {
        if (!$this->readable) {
            throw new IllegalStateException('Handle is not readable!');
        }

        if ($this->closed) {
            throw new IllegalStateException('Handle has been closed!');
        }

        if ($this->isEOF()) {
            return null;
        }

        $read = @fgets($this->resource);

        if ($read === false) {
            throw IOException::fromLastError();
        }

        $read = preg_replace("/\r?\n$/", '', $read);

        return $encoding === Encoding::UTF8 ? $read : mb_convert_encoding($read, Encoding::UTF8, $encoding);
    }

    /**
     * {@inheritdoc}
     */
    public function lines(string $encoding = Encoding::UTF8): \Generator
    {
        if (!$this->readable) {
            throw new IllegalStateException('Handle is not readable!');
        }

        if ($this->closed) {
            throw new IllegalStateException('Handle has been closed!');
        }

        $notDefaultEncoding = $encoding !== Encoding::UTF8;

        while ($l = @fgets($this->resource)) {
            if ($l === false && error_get_last() !== null) {
                throw IOException::fromLastError();
            }

            if ($notDefaultEncoding) {
                $l = mb_convert_encoding($l, Encoding::UTF8, $encoding);
            }

            yield preg_replace("/\r?\n$/", '', $l);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function copyTo($handle, int $bufferSize = 4096): int
    {
        if (!$this->readable) {
            throw new IllegalStateException('Handle is not readable!');
        }

        if ($this->closed) {
            throw new IllegalStateException('Handle has been closed!');
        }

        $written = 0;

        if ($handle instanceof StreamInterface) {
            if ($handle->isClosed()) {
                throw new IllegalStateException('Cannot write to a closed stream!');
            }

            if (!$handle->isWritable()) {
                throw new IllegalStateException('Cannot write to a non-writable stream!');
            }

            while (($buffer = $this->readBytes($bufferSize)) !== null) {
                $written += $handle->writeBytes($buffer);
            }
        } elseif (\is_resource($handle)) {
            while (!feof($this->resource)) {
                $buffer = @fread($this->resource, $bufferSize);

                if ($buffer === false) {
                    throw IOException::fromLastError();
                }

                $buffer = @fwrite($handle, $buffer);

                if ($buffer === false) {
                    throw IOException::fromLastError();
                }

                $written += $buffer;
            }
        } else {
            throw new \InvalidArgumentException('The provided handle is neither a StreamInterface nor a resource!');
        }

        return $written;
    }

    /**
     * {@inheritdoc}
     */
    public function seek(int $offset, int $position = SeekPosition::START): StreamInterface
    {
        if ($this->closed) {
            throw new IllegalStateException('Handle has been closed!');
        }

        if (!$this->seekable) {
            throw new IllegalStateException('This stream is not seekable!');
        }

        switch ($position) {
            case SeekPosition::START:
                if ($offset < 0) {
                    throw new ArgumentOutOfRangeException('Position must be greater than or equal to zero!');
                }

                $seekResult = @fseek($this->resource, $offset);
                break;
            case SeekPosition::CURSOR:
                $seekResult = @fseek($this->resource, $offset, SEEK_CUR);
                break;
            case SeekPosition::END:
                if ($offset < 0) {
                    throw new ArgumentOutOfRangeException('Position must be greater than or equal to zero!');
                }

                $seekResult = @fseek($this->resource, -$offset, SEEK_END);
                break;
            default:
                throw new \InvalidArgumentException('Invalid seek position provided!');
        }

        if ($seekResult === -1 || $seekResult === false) {
            throw IOException::fromLastError();
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function size(): ?int
    {
        if (!$this->seekable) {
            return null;
        }

        // Get the stream's position to restore it later.
        $initialPosition = $this->tell();

        // Seek the end of the stream.
        $this->seek(0, SeekPosition::END);

        // Get the cursor position (which is the stream size in bytes).
        $size = $this->tell();

        // Restore the previous position.
        $this->seek($initialPosition);

        return $size;
    }

    /**
     * {@inheritdoc}
     */
    public function tell(): int
    {
        $pos = @ftell($this->resource);

        if ($pos === false) {
            throw IOException::fromLastError();
        }

        return $pos;
    }

    /**
     * {@inheritdoc}
     */
    public function flush(): void
    {
        if (!$this->writable) {
            throw new IllegalStateException('Handle is not writable!');
        }

        if ($this->closed) {
            throw new IllegalStateException('Handle has been closed!');
        }

        if (!@fflush($this->resource)) {
            throw IOException::fromLastError();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function close(): void
    {
        if (!$this->closed) {
            if (!@fclose($this->resource)) {
                throw IOException::fromLastError();
            }

            $this->closed = true;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function asPsrStream(): \Psr\Http\Message\StreamInterface
    {
        try {
            return new PsrResourceStreamAdapter(new self($this->getResource()));
        } finally {
            $this->resource = null;
            $this->closed   = true;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isEOF(): bool
    {
        if (!$this->readable) {
            throw new IllegalStateException('This stream is not readable!');
        }

        if (!$this->seekable) {
            throw new IllegalStateException('This stream is not seekable!');
        }

        if ($this->closed) {
            throw new IllegalStateException('This stream has been closed!');
        }

        if (fgetc($this->resource) === false) {
            return true;
        }

        $this->seek(-1, SeekPosition::CURSOR);

        return false;
    }

    /**
     * @return resource the underlying resource
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @return array the underlying resource's metadata
     */
    public function getStreamMetadata(): array
    {
        if ($this->closed) {
            throw new IllegalStateException('The stream has been closed!');
        }

        return stream_get_meta_data($this->resource);
    }

    /**
     * {@inheritdoc}
     */
    public function isSeekable(): bool
    {
        return $this->seekable;
    }

    /**
     * {@inheritdoc}
     */
    public function isReadable(): bool
    {
        return $this->readable;
    }

    /**
     * {@inheritdoc}
     */
    public function isWritable(): bool
    {
        return $this->writable;
    }

    /**
     * {@inheritdoc}
     */
    public function isClosed(): bool
    {
        return $this->closed;
    }
}
