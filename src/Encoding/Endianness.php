<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\Encoding;

/**
 * Enum for endianness.
 */
final class Endianness
{
    /**
     * Little endian.
     */
    public const LITTLE = 0;

    /**
     * Big endian.
     */
    public const BIG = 1;

    /**
     * @var null|int the current system's endianness
     */
    private static $CURRENT;

    private function __construct()
    {
    }

    /**
     * Converts a 32-bit integer's endianness.
     *
     * @param int $value          the integer to convert
     * @param int $fromEndianness the source endianness
     * @param int $toEndianness   the targeted endianness
     *
     * @return int the converted value
     */
    public static function convertShort(int $value, int $fromEndianness, int $toEndianness): int
    {
        if ($fromEndianness === $toEndianness) {
            return $value;
        }

        return
            (($value & 0x00FF) << 8) |
            (($value & 0xFF00) >> 8);
    }

    /**
     * Converts a 64-bit integer's endianness.
     *
     * @param int $value          the integer to convert
     * @param int $fromEndianness the source endianness
     * @param int $toEndianness   the targeted endianness
     *
     * @return int the converted value
     */
    public static function convertInt(int $value, int $fromEndianness, int $toEndianness): int
    {
        if ($fromEndianness === $toEndianness) {
            return $value;
        }

        return
            (($value & 0x000000FF) << 24) |
            (($value & 0x0000FF00) << 8) |
            (($value & 0x00FF0000) >> 8) |
            (($value & 0xFF000000) >> 24);
    }

    /**
     * @return int the system's current endianness
     */
    public static function getCurrent(): int
    {
        return self::$CURRENT ?? (self::$CURRENT = (int) (pack('L', 1) === pack('N', 1)));
    }
}
