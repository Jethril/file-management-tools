<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\Encoding;

/**
 * Common BOMs.
 */
final class ByteOrderMark
{
    /**
     * The UTF-8 BOM.
     */
    public const UTF8 = "\xEF\xBB\xBF";

    /**
     * The UTF-16LE BOM.
     */
    public const UTF16_LE = "\xFF\xFE";

    /**
     * The UTF-16BE BOM.
     */
    public const UTF16_BE = "\xFE\xFF";

    /**
     * The UTF-32LE BOM.
     */
    public const UTF32_LE = "\xFF\xFE\x00\x00";

    /**
     * The UTF-32BE BOM.
     */
    public const UTF32_BE = "\x00\x00\xFE\xFF";

    private function __construct()
    {
    }
}
