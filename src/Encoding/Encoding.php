<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\Encoding;

/**
 * Lists some common encoding, prevents mistakes when using native methods.
 */
final class Encoding
{
    /**
     * ASCII encoding.
     */
    public const ASCII = 'ASCII';

    /**
     * UTF-8 encoding.
     */
    public const UTF8 = 'UTF-8';

    /**
     * UTF-16LE encoding.
     */
    public const UTF16_LE = 'UTF-16LE';

    /**
     * UTF-16BE encoding.
     */
    public const UTF16_BE = 'UTF-16BE';

    /**
     * UTF-32LE encoding.
     */
    public const UTF32_LE = 'UTF-32LE';

    /**
     * UTF-32BE encoding.
     */
    public const UTF32_BE = 'UTF-32BE';

    /**
     * Windows-1252 encoding.
     */
    public const WINDOWS_1252 = 'Windows-1252';

    private function __construct()
    {
    }
}
