<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\TypeSniffer;

/**
 * Represents a file type.
 */
final class FileType
{
    /**
     * Unknown family type.
     */
    public const FAMILY_UNKNOWN = 0;

    /**
     * Includes all archive formats (ISO, tar, bzip...).
     */
    public const FAMILY_ARCHIVE = 1;

    /**
     * Includes all document formats (xls, csv, doc...).
     */
    public const FAMILY_DOCUMENT = 3;

    /**
     * Includes all audio, fonts and video formats.
     */
    public const FAMILY_MEDIA = 7;

    /**
     * Includes all binary files (executables, DDLs, SSH keys...).
     */
    public const FAMILY_BINARIES = 15;

    /**
     * Complete list of families.
     */
    public const FAMILIES = [
        self::FAMILY_UNKNOWN,
        self::FAMILY_ARCHIVE,
        self::FAMILY_DOCUMENT,
        self::FAMILY_MEDIA,
        self::FAMILY_BINARIES,
    ];

    public const UA   = 0 << 4 | self::FAMILY_ARCHIVE;
    public const TAR  = 1 << 4 | self::FAMILY_ARCHIVE;
    public const ISO  = 2 << 4 | self::FAMILY_ARCHIVE;
    public const BZ2  = 3 << 4 | self::FAMILY_ARCHIVE;
    public const GZ   = 4 << 4 | self::FAMILY_ARCHIVE;
    public const LZ   = 5 << 4 | self::FAMILY_ARCHIVE;
    public const LZMA = 6 << 4 | self::FAMILY_ARCHIVE;
    public const CAB  = 7 << 4 | self::FAMILY_ARCHIVE;
    public const RAR  = 8 << 4 | self::FAMILY_ARCHIVE;
    public const ZIP  = 9 << 4 | self::FAMILY_ARCHIVE;

    public const WAV  = 10 << 4 | self::FAMILY_MEDIA;
    public const AIFF = 11 << 4 | self::FAMILY_MEDIA;
    public const FLAC = 12 << 4 | self::FAMILY_MEDIA;
    public const MIDI = 13 << 4 | self::FAMILY_MEDIA;
    public const WMA  = 14 << 4 | self::FAMILY_MEDIA;
    public const MP3  = 15 << 4 | self::FAMILY_MEDIA;
    public const OGG  = 16 << 4 | self::FAMILY_MEDIA;
    public const MPEG = 17 << 4 | self::FAMILY_MEDIA;
    public const AVI  = 18 << 4 | self::FAMILY_MEDIA;

    public const DOC  = 19 << 4 | self::FAMILY_DOCUMENT;
    public const XLS  = 20 << 4 | self::FAMILY_DOCUMENT;
    public const PPT  = 21 << 4 | self::FAMILY_DOCUMENT;
    public const DOCX = 22 << 4 | self::FAMILY_DOCUMENT;
    public const XLSX = 23 << 4 | self::FAMILY_DOCUMENT;
    public const PPTX = 24 << 4 | self::FAMILY_DOCUMENT;
    public const CSV  = 25 << 4 | self::FAMILY_DOCUMENT;
    public const TSV  = 26 << 4 | self::FAMILY_DOCUMENT;

    public const EXE = 27 << 4 | self::FAMILY_BINARIES;
    public const DLL = 28 << 4 | self::FAMILY_BINARIES;
    public const ELF = 29 << 4 | self::FAMILY_BINARIES;

    /**
     * @var string the type's name
     */
    private $name;

    /**
     * @var string[] the type's extensions
     */
    private $extensions;

    /**
     * @var string[] the type's mimes
     */
    private $mimeTypes;

    /**
     * @var int the type's code
     */
    private $code;

    /**
     * Creates a new FileType.
     *
     * @param string   $name       the type's name
     * @param string[] $extensions the type's extensions
     * @param string[] $mimeTypes  the type's mimes
     * @param int      $code       the type's code
     */
    public function __construct(string $name, array $extensions, array $mimeTypes, int $code)
    {
        $this->name       = $name;
        $this->extensions = $extensions;
        $this->mimeTypes  = $mimeTypes;
        $this->code       = $code;
    }

    /**
     * @return string the type's name
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string[] the type's extensions
     */
    public function getExtensions(): array
    {
        return $this->extensions;
    }

    /**
     * @return string[] the mime types
     */
    public function getMimeTypes(): array
    {
        return $this->mimeTypes;
    }

    /**
     * @return int the type's family
     */
    public function getFamily(): int
    {
        return $this->code & 15;
    }

    /**
     * @return int the type's code
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @return bool `true` if this type is unknown, `false` otherwise
     */
    public function isUnknown(): bool
    {
        return $this->getFamily() === self::FAMILY_UNKNOWN;
    }
}
