<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\Exceptions;

/**
 * Thrown when conditional statements encounter unhandled values.
 *
 * Note: this exception is meant to signal bugs (used like an assertion) and should not be used to validate arguments!
 */
class UnhandledValueException extends \RuntimeException
{
    /**
     * Constructs a new UnhandledValueException.
     *
     * @param mixed $value the value that wasn't handled
     */
    public function __construct($value)
    {
        $value = var_export($value, true);

        parent::__construct("Unhandled value: '{$value}'!");
    }
}
