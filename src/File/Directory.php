<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\File;

use FileManagementTools\File\Exceptions\DirectoryNotFoundException;
use FileManagementTools\File\Exceptions\ElementAlreadyExistsException;
use FileManagementTools\File\Exceptions\IOException;

/**
 * Allows managing directories with clear exceptions.
 */
final class Directory
{
    /**
     * Restricts directory listing to subdirectories only.
     */
    public const LIST_DIRECTORIES = 0;

    /**
     * Restricts directory listing to files only.
     */
    public const LIST_FILES = 1;

    /**
     * Does not restrict directory listing.
     */
    public const LIST_BOTH = 2;

    private function __construct()
    {
    }

    /**
     * Tests whether the provided path is an existing directory.
     *
     * @param string $path        the path to test
     * @param bool   $ignoreCache `true` if the PHP cache should be ignored, `false` otherwise
     *
     * @return bool `true` if the path is a valid directory, `false` otherwise
     */
    public static function exists(string $path, bool $ignoreCache = false): bool
    {
        if ($ignoreCache) {
            clearstatcache(true, $path);
        }

        return is_dir($path);
    }

    /**
     * Creates a new directory.
     *
     * @param string $path           the directory path
     * @param bool   $ignoreExisting `true` if the method should fail on existing directories, `false` otherwise
     *
     * @throws IOException
     */
    public static function create(string $path, bool $ignoreExisting = false): void
    {
        if (is_file($path)) {
            throw new ElementAlreadyExistsException("Path '{$path}' already exists and is a file!");
        }

        if (is_dir($path)) {
            if ($ignoreExisting) {
                return;
            }

            throw new ElementAlreadyExistsException("Directory '{$path}' already exists!");
        }

        if (!(@mkdir($path, 0775)) && !is_dir($path)) {
            throw IOException::fromLastError();
        }
    }

    /**
     * Creates all directories until the provided path, recursively.
     *
     * @param string $path the path to create
     *
     * @throws IOException
     */
    public static function createAll(string $path): void
    {
        if (is_dir($path)) {
            return;
        }

        if (is_file($path)) {
            throw new ElementAlreadyExistsException("Path '{$path}' already exists and is a file!");
        }

        if (!(@mkdir($path, 0775, true)) && !is_dir($path)) {
            throw IOException::fromLastError();
        }
    }

    /**
     * Moves a directory recursively.
     *
     * Note: does not moves symlinks.
     *
     * The destination could be:
     *  - An existing directory, so the directory will be moved inside it and keep its current name.
     *  - A non-existing directory, so the directory will be created and the contents of the source will be moved in it.
     *
     * @param string $source      the source directory
     * @param string $destination the destination directory
     * @param bool   $followLinks if the recursion should visit symlinks
     *
     * @throws IOException
     */
    public static function move(string $source, string $destination, bool $followLinks = false): void
    {
        if (!is_dir($source)) {
            throw new DirectoryNotFoundException($source);
        }

        if (is_dir($destination)) {
            $destination = Path::join($destination, Path::getFileName($source));
        }

        self::create($destination);

        foreach (self::listEntriesToArray($source) as $entry) {
            $src = Path::join($source, $entry);

            if (!$followLinks && is_link($src)) {
                continue;
            }

            $dst = Path::join($destination, $entry);

            if (is_dir($src)) {
                self::move($src, $dst, $followLinks);
            } else {
                File::move($src, $dst);
            }
        }

        if (!@rmdir($source)) {
            throw IOException::fromLastError();
        }
    }

    /**
     * Copies a directory recursively.
     *
     * Note: does not copies symlinks.
     *
     * The destination could be:
     *  - An existing directory, so the directory will be copied inside it and keep its current name.
     *  - A non-existing directory, so the directory will be created and the contents of the source will be copied in
     *    it.
     *
     * @param string $source      the source directory
     * @param string $destination the destination directory
     * @param bool   $followLinks if the recursion should visit symlinks
     *
     * @throws IOException
     */
    public static function copy(string $source, string $destination, bool $followLinks = false): void
    {
        if (!is_dir($source)) {
            throw new DirectoryNotFoundException($source);
        }

        if (is_dir($destination)) {
            $destination = Path::join($destination, Path::getFileName($source));
        }

        self::create($destination);

        foreach (self::listEntriesToArray($source, true, self::LIST_BOTH, $followLinks) as $entry) {
            $src = Path::join($source, $entry);

            if (!$followLinks && is_link($src)) {
                continue;
            }

            $dst = Path::join($destination, $entry);

            if (is_dir($src)) {
                self::copy($src, $dst, $followLinks);
            } else {
                File::copy($src, $dst);
            }
        }
    }

    /**
     * Recursively deletes a directory.
     *
     * @param string $path              the directory to delete
     * @param bool   $ignoreNonExisting `true` if this function should throw an exception when the directory does not
     *                                  exists, `false` otherwise
     *
     * @throws IOException
     */
    public static function delete(string $path, bool $ignoreNonExisting = false): void
    {
        if (!is_dir($path)) {
            if ($ignoreNonExisting) {
                return;
            }

            throw new DirectoryNotFoundException($path);
        }

        if (is_link($path)) {
            if (!@rmdir($path)) {
                throw IOException::fromLastError();
            }

            return;
        }

        foreach (self::listEntriesToArray($path) as $entry) {
            $entry = Path::join($path, $entry);

            if (is_dir($entry)) {
                self::delete($entry);
            } elseif (is_file($entry)) {
                File::delete($entry);
            } elseif (is_link($entry)) {
                Link::delete($entry);
            }
        }

        if (!@rmdir($path)) {
            throw IOException::fromLastError();
        }
    }

    /**
     * Returns all directory's children in an array (can be recursive).
     *
     * @param string $path        the directory's path
     * @param bool   $recursive   whether to list the entries recursively
     * @param int    $filter      one of the {@see Directory}::LIST_* constants
     * @param bool   $followLinks whether to follow links
     *
     * @throws IOException
     *
     * @return string[] the children's paths (relative to $path)
     */
    public static function listEntriesToArray(
        string $path,
        bool $recursive = false,
        int $filter = self::LIST_BOTH,
        bool $followLinks = true
    ): array {
        return iterator_to_array(self::listEntries($path, $recursive, $filter, $followLinks), false);
    }

    /**
     * Creates an iterator on all directory's children (can be recursive).
     *
     * @param string $path        the directory's path
     * @param bool   $recursive   whether to list the entries recursively
     * @param int    $filter      one of the {@see Directory}::LIST_* constants
     * @param bool   $followLinks whether to follow links
     *
     * @throws DirectoryNotFoundException
     * @throws IOException
     *
     * @return \Generator|string[] the children's paths (relative to $path)
     */
    public static function listEntries(
        string $path,
        bool $recursive = false,
        int $filter = self::LIST_BOTH,
        bool $followLinks = true
    ): \Generator {
        if (!is_dir($path)) {
            throw new DirectoryNotFoundException($path);
        }

        switch ($filter) {
            case self::LIST_DIRECTORIES:
                $directories = true;
                $files       = false;

                break;
            case self::LIST_FILES:
                $directories = false;
                $files       = true;

                break;
            case self::LIST_BOTH:
                $directories = true;
                $files       = true;

                break;
            default:
                throw new \InvalidArgumentException('Unknown filter constant provided!');
        }

        return self::listEntriesInternal($path, '', $files, $directories, $recursive, $followLinks);
    }

    /**
     * Internal method, {@see Directory::listEntries()} for details.
     *
     * @param string $path         the directory's path
     * @param string $relativePath the path relative to the initial entriesInternal() call
     * @param bool   $files        whether to include files
     * @param bool   $directories  whether to include directories
     * @param bool   $recursive    whether to list the entries recursively
     * @param bool   $followLinks  whether to follow links
     *
     * @throws IOException
     *
     * @return \Generator|string[] the children's paths (relative to $relativePath)
     */
    private static function listEntriesInternal(
        string $path,
        string $relativePath,
        bool $files,
        bool $directories,
        bool $recursive,
        bool $followLinks
    ): \Generator {
        $entries = @scandir($path);

        if ($entries === false) {
            throw IOException::fromLastError();
        }

        foreach ($entries as $entry) {
            if ($entry === '.' || $entry === '..') {
                continue;
            }

            $entryPath = Path::join($path, $entry);

            if (is_dir($entryPath)) {
                if ($directories) {
                    yield Path::join($relativePath, $entry);
                }

                if ($recursive && ($followLinks || !is_link($entryPath))) {
                    yield from self::listEntriesInternal(
                        $entryPath,
                        Path::join($relativePath, Path::getFileName($entry)),
                        $files,
                        $directories,
                        true,
                        $followLinks
                    );
                }
            } elseif ($files && is_file($entryPath)) {
                yield Path::join($relativePath, $entry);
            }
        }
    }
}
