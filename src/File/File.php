<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\File;

use FileManagementTools\Encoding\Encoding;
use FileManagementTools\File\Exceptions\DirectoryNotFoundException;
use FileManagementTools\File\Exceptions\ElementAlreadyExistsException;
use FileManagementTools\File\Exceptions\FileNotFoundException;
use FileManagementTools\File\Exceptions\FileNotReadableException;
use FileManagementTools\File\Exceptions\FileNotWritableException;
use FileManagementTools\File\Exceptions\IOException;
use FileManagementTools\File\Exceptions\TruncatedDataException;
use FileManagementTools\Stream\ResourceStream;
use FileManagementTools\Stream\StreamInterface;
use FileManagementTools\TypeSniffer\FileType;
use FileManagementTools\TypeSniffer\FileTypeSnifferRegistry;

/**
 * Helps managing files using proper error handling and modern data streams.
 */
final class File
{
    private function __construct()
    {
    }

    /**
     * Tests whether the provided path is a valid file.
     *
     * @param string $path        the path to test
     * @param bool   $ignoreCache `true` if the PHP cache should be ignored, `false` otherwise
     *
     * @return bool `true` if the path is a valid file, `false` otherwise
     */
    public static function exists(string $path, bool $ignoreCache = false): bool
    {
        if ($ignoreCache) {
            clearstatcache(true, $path);
        }

        return is_file($path);
    }

    /**
     * Safely opens a file in read-mode.
     *
     * @param string $path the file's path
     *
     * @throws IOException
     *
     * @return StreamInterface the file handle
     */
    public static function openRead(string $path): StreamInterface
    {
        if (!is_file($path)) {
            throw new FileNotFoundException($path);
        }

        if (!is_readable($path)) {
            throw new FileNotReadableException($path);
        }

        $handle = @fopen($path, 'rb');

        if (!$handle) {
            throw IOException::fromLastError();
        }

        return new ResourceStream($handle, true, false);
    }

    /**
     * Safely opens a file in write-mode.
     *
     * @param string $path   the file's path
     * @param bool   $create if the file should be created if it does not exists
     * @param bool   $append if the data should be appended or the file should be truncated from its beginning
     *
     * @throws IOException
     *
     * @return StreamInterface the file handle
     */
    public static function openWrite(string $path, bool $create = true, bool $append = false): StreamInterface
    {
        $exists = is_file($path);

        if (!$create && !$exists) {
            throw new FileNotFoundException($path);
        }

        if ($exists && !is_writable($path)) {
            throw new FileNotWritableException($path);
        }

        $handle = @fopen($path, $append ? 'ab' : 'wb');

        if (!$handle) {
            throw IOException::fromLastError();
        }

        return new ResourceStream($handle, false, true);
    }

    /**
     * Safely opens a file in read/write mode.
     *
     * @param string $path   the file's path
     * @param bool   $create if the file should be created if it does not exists
     * @param bool   $append if the data should be appended or the file should be truncated from its beginning
     *
     * @throws IOException
     *
     * @return StreamInterface the file handle
     */
    public static function openReadWrite(string $path, bool $create = true, bool $append = false): StreamInterface
    {
        $exists = is_file($path);

        if (!$create && !$exists) {
            throw new FileNotFoundException($path);
        }

        if ($exists && !is_readable($path)) {
            throw new FileNotReadableException($path);
        }

        if ($exists && !is_writable($path)) {
            throw new FileNotWritableException($path);
        }

        $handle = fopen($path, $append ? 'ab+' : 'wb+');

        if (!$handle) {
            throw IOException::fromLastError();
        }

        return new ResourceStream($handle, true, true);
    }

    /**
     * Copies a file to a handle / stream.
     *
     * @param string                   $path     the file's path
     * @param resource|StreamInterface $resource a valid PHP resource or a writable StreamInterface
     *
     * @throws IOException
     *
     * @return int the number of bytes that were copied
     */
    public static function copyTo(string $path, $resource): int
    {
        if ($resource instanceof StreamInterface) {
            if (!$resource->isWritable()) {
                throw new \InvalidArgumentException('The provided stream must be writable!');
            }
        } elseif (!\is_resource($resource)) {
            $type = \is_object($resource) ? \get_class($resource) : \gettype($resource);

            throw new \InvalidArgumentException(
                "The provided resource must be either a resource or a StreamInterface, {$type} given!"
            );
        }

        $handle = self::openRead($path);

        try {
            return $handle->copyTo($resource);
        } finally {
            $handle->close();
        }
    }

    /**
     * Reads a number of bytes from a file.
     *
     * @param string $path        the file path
     * @param int    $offset      the offset (set to 0 to read from the beginning)
     * @param int    $length      the length to read
     * @param bool   $checkLength If the size of the output should be checked against the provided length. Throws a
     *                            TruncatedDataException if the number of bytes that have been read is different from
     *                            the provided length.
     *
     * @throws IOException
     * @throws TruncatedDataException
     *
     * @return string a binary string containing the bytes read
     */
    public static function readBytes(string $path, int $offset, int $length, bool $checkLength = false): string
    {
        if (!is_file($path)) {
            throw new FileNotFoundException($path);
        }

        if (!is_readable($path)) {
            throw new FileNotReadableException($path);
        }

        $result = @file_get_contents($path, false, null, $offset, $length);

        if ($result === false) {
            throw IOException::fromLastError();
        }

        if ($checkLength && \strlen($result) !== $length) {
            throw new TruncatedDataException($path, $offset, $length);
        }

        return $result;
    }

    /**
     * Reads the whole file.
     *
     * @param string $path the file path
     *
     * @throws FileNotReadableException
     * @throws IOException
     * @throws FileNotFoundException
     *
     * @return string a binary string containing the file's bytes
     */
    public static function readAllBytes(string $path): string
    {
        if (!is_file($path)) {
            throw new FileNotFoundException($path);
        }

        if (!is_readable($path)) {
            throw new FileNotReadableException($path);
        }

        $result = @file_get_contents($path);

        if ($result === false) {
            throw IOException::fromLastError();
        }

        return $result;
    }

    /**
     * Reads all text from a file.
     *
     * @param string $file     the file to read the text from
     * @param string $encoding the text encoding
     *
     * @throws IOException
     *
     * @return string the text read
     */
    public static function readAllText(string $file, string $encoding = Encoding::UTF8): string
    {
        if (!is_file($file)) {
            throw new FileNotFoundException($file);
        }

        $result = @file_get_contents($file);

        if ($result === false) {
            throw IOException::fromLastError();
        }

        return $encoding === Encoding::UTF8 ? $result : mb_convert_encoding($result, Encoding::UTF8, $encoding);
    }

    /**
     * Lazily loads all lines from the provided file.
     * The file is automatically closed when the generator is closed.
     *
     * @param string $file     the file
     * @param string $encoding the file's encoding
     *
     * @throws IOException
     *
     * @return \Generator|string[] the file's lines
     */
    public static function lines(string $file, string $encoding = Encoding::UTF8): \Generator
    {
        if (!is_file($file)) {
            throw new FileNotFoundException($file);
        }

        $handle = null;

        try {
            $handle = @fopen($file, 'rb');

            if ($handle === false) {
                IOException::fromLastError();
            }

            $notDefaultEncoding = $encoding !== Encoding::UTF8;

            while ($l = @fgets($handle)) {
                if ($l === false && error_get_last() !== null) {
                    throw IOException::fromLastError();
                }

                if ($notDefaultEncoding) {
                    $l = mb_convert_encoding($l, Encoding::UTF8, $encoding);
                }

                yield preg_replace("/\r?\n$/", '', $l);
            }
        } finally {
            if (\is_resource($handle) && !@fclose($handle)) {
                throw IOException::fromLastError();
            }
        }
    }

    /**
     * Writes the provided bytes to the supplied file.
     *
     * @param string $path   the file path
     * @param string $bytes  the bytes to write
     * @param bool   $create if the file should be created if it does not exists
     *
     * @throws IOException
     */
    public static function writeAllBytes(string $path, string $bytes, bool $create = true): void
    {
        if (!$create && !is_file($path)) {
            throw new FileNotFoundException($path);
        }

        if ((@file_put_contents($path, $bytes)) === false) {
            throw IOException::fromLastError();
        }
    }

    /**
     * Appends the provided bytes to the supplied file.
     *
     * @param string $path   the file path
     * @param string $bytes  the bytes to write
     * @param bool   $create if the file should be created if it does not exists
     *
     * @throws IOException
     */
    public static function appendBytes(string $path, string $bytes, bool $create = true): void
    {
        if (!$create && !is_file($path)) {
            throw new FileNotFoundException($path);
        }

        if ((@file_put_contents($path, $bytes, FILE_APPEND)) === false) {
            throw IOException::fromLastError();
        }
    }

    /**
     * Writes the provided text to the file.
     *
     * @param string $file     the file to write the text to
     * @param string $text     the text to write
     * @param bool   $create   if the file should be created if it does not exists
     * @param string $encoding the target encoding
     *
     * @throws IOException
     */
    public static function writeAllText(
        string $file,
        string $text,
        bool $create = true,
        string $encoding = Encoding::UTF8
    ): void {
        if (!$create && !is_file($file)) {
            throw new FileNotFoundException($file);
        }

        if ($encoding !== Encoding::UTF8) {
            $text = mb_convert_encoding($text, $encoding, Encoding::UTF8);
        }

        if ((@file_put_contents($file, $text)) === false) {
            throw IOException::fromLastError();
        }
    }

    /**
     * Appends the provided text to the end of the file.
     *
     * @param string $file     the file to append the text to
     * @param string $text     the text to append
     * @param bool   $create   if the file should be created if it does not exists
     * @param string $encoding the target encoding
     *
     * @throws IOException
     */
    public static function appendText(
        string $file,
        string $text,
        bool $create = true,
        string $encoding = Encoding::UTF8
    ): void {
        if (!$create && !is_file($file)) {
            throw new FileNotFoundException($file);
        }

        if ($encoding !== Encoding::UTF8) {
            $text = mb_convert_encoding($text, $encoding, Encoding::UTF8);
        }

        if ((@file_put_contents($file, $text, FILE_APPEND)) === false) {
            throw IOException::fromLastError();
        }
    }

    /**
     * Opens the provided file in read mode and passes the handle to the supplied callback.
     * Ensures that the file is closed when the callback is finished (exceptionally or not).
     * The returned value is the callback's return value.
     *
     * @param string   $path     the file's path
     * @param callable $callback the callback
     *
     * @throws IOException
     *
     * @return mixed the value returned by the callback
     */
    public static function reading(string $path, callable $callback)
    {
        $stream = self::openRead($path);

        try {
            return $callback($stream);
        } finally {
            $stream->close();
        }
    }

    /**
     * Opens the provided file in write mode and passes the handle to the supplied callback.
     * Ensures that the file is closed when the callback is finished (exceptionally or not).
     * The returned value is the callback's return value.
     *
     * @param string   $path     the file's path
     * @param callable $callback the callback
     *
     * @throws IOException
     *
     * @return mixed the value returned by the callback
     */
    public static function writing(string $path, callable $callback)
    {
        $handle = self::openWrite($path);

        try {
            return $callback($handle);
        } finally {
            $handle->flush();
            $handle->close();
        }
    }

    /**
     * Opens the provided file in read/write mode and passes the handle to the supplied callback.
     * Ensures that the file is closed when the callback is finished (exceptionally or not).
     * The returned value is the callback's return value.
     *
     * @param string   $path     the file's path
     * @param callable $callback the callback
     *
     * @throws IOException
     *
     * @return mixed the value returned by the callback
     */
    public static function readingWriting(string $path, callable $callback)
    {
        $handle = self::openReadWrite($path);

        try {
            return $callback($handle);
        } finally {
            $handle->flush();
            $handle->close();
        }
    }

    /**
     * Creates a temporary file, opens it and passes its handle as the first callback parameter and its path as the
     * second one. Ensures that the handle is closed and deleted at the end of callback. Finally returns the callback's
     * return value.
     *
     * @param callable    $callback  the callback
     * @param null|string $directory the directory where the temporary file should be created
     *
     * @throws IOException
     *
     * @return mixed the value returned by the callback
     */
    public static function temporarily(callable $callback, ?string $directory = null)
    {
        $path   = '';
        $handle = self::createTemporary($path, $directory);

        try {
            return $callback($handle, $path);
        } finally {
            $handle->flush();
            $handle->close();
            self::delete($path);
        }
    }

    /**
     * Creates the provided file.
     *
     * @param string $path           the file path
     * @param bool   $ignoreExisting if the existence of the file should be ignored (do not throws an exception if
     *                               true)
     *
     * @throws IOException
     */
    public static function create(string $path, bool $ignoreExisting = false): void
    {
        if (is_dir($path)) {
            throw new ElementAlreadyExistsException("Path '{$path}' exists and is a directory!");
        }

        if (is_file($path)) {
            if ($ignoreExisting) {
                return;
            }

            throw new ElementAlreadyExistsException("File '{$path}' already exists!");
        }

        if (!@touch($path)) {
            throw IOException::fromLastError();
        }
    }

    /**
     * Creates a temporary file. Concurrently safe.
     *
     * @param string      $path      a variable where the generated file path will be written
     * @param null|string $directory The directory where the file will be created. Defaults to the system's temp dir.
     *
     * @throws IOException
     *
     * @return StreamInterface a read/write stream on the file
     */
    public static function createTemporary(string &$path, ?string $directory = null): StreamInterface
    {
        $directory = $directory ?? sys_get_temp_dir();

        if (!is_dir($directory)) {
            throw new DirectoryNotFoundException($directory);
        }

        $attempts  = 0;
        $handle    = null;
        $lastError = null;

        do {
            try {
                $path   = Path::join(sys_get_temp_dir(), base64_encode(random_bytes(8)) . '.tmp');
                $handle = @fopen($path, 'wb+');
            } catch (\Exception $e) {
                // No choice, we must catch "Exception"...
                throw new IOException('Unable to generate random bytes!', $e->getCode(), $e);
            }

            if ($handle === false) {
                $handle    = null;
                $lastError = IOException::fromLastError();
            }
        } while ($lastError !== null && ++$attempts < 20);

        if ($handle === null) {
            throw $lastError;
        }

        return new ResourceStream($handle, true, true);
    }

    /**
     * Moves a source file to the provided destination.
     *
     * The destination could be:
     *  - A directory, so the file will be moved inside it and keep its current name.
     *  - An (non-)existing file, so the file will be overwritten with the source's contents.
     *
     * @param string $source      the source file
     * @param string $destination the destination
     * @param bool   $overwrite   Should we overwrite an existing destination?
     *
     * @throws IOException
     */
    public static function move(string $source, string $destination, bool $overwrite = true): void
    {
        if (!is_file($source)) {
            throw new FileNotFoundException($source);
        }

        if (is_dir($destination)) {
            $destination = Path::join($destination, Path::getFileName($source));
        }

        if (is_file($destination)) {
            if (!$overwrite) {
                throw new ElementAlreadyExistsException("File '{$destination}' already exists!");
            }

            self::delete($destination);
        }

        if (!@rename($source, $destination)) {
            throw IOException::fromLastError();
        }
    }

    /**
     * Copies a source file to the provided destination.
     *
     * The destination could be:
     *  - A directory, so the file will be copied inside it and keep its current name.
     *  - An (non-)existing file, so the file will be overwritten with the source's contents.
     *
     * @param string $source      the source file
     * @param string $destination the destination
     * @param bool   $overwrite   Should we overwrite an existing destination?
     *
     * @throws IOException
     */
    public static function copy(string $source, string $destination, bool $overwrite = true): void
    {
        if (!is_file($source)) {
            throw new FileNotFoundException($source);
        }

        if (is_dir($destination)) {
            $destination = Path::join($destination, Path::getFileName($source));
        }

        if (!$overwrite && is_file($destination)) {
            throw new ElementAlreadyExistsException("File '{$destination}' already exists!");
        }

        if (!@copy($source, $destination)) {
            throw IOException::fromLastError();
        }
    }

    /**
     * Deletes the provided file.
     *
     * @param string $path              the file path
     * @param bool   $ignoreNonExisting if the absence of the file should be ignored (do not throws an exception if
     *                                  true)
     *
     * @throws IOException
     */
    public static function delete(string $path, bool $ignoreNonExisting = false): void
    {
        if (is_dir($path)) {
            throw new IOException("Path '{$path}' exists but is a directory!");
        }

        if (!is_file($path)) {
            if ($ignoreNonExisting) {
                return;
            }

            throw new FileNotFoundException($path);
        }

        if (!@unlink($path)) {
            throw IOException::fromLastError();
        }
    }

    /**
     * Returns the size of file.
     *
     * @param string $path        the file path
     * @param bool   $ignoreCache if the stat cache should be bypassed
     *
     * @throws IOException
     *
     * @return int the file's size, in bytes
     */
    public static function getSize(string $path, bool $ignoreCache = false): int
    {
        if (!is_file($path)) {
            throw new FileNotFoundException($path);
        }

        if ($ignoreCache) {
            clearstatcache(true, $path);
        }

        $result = @filesize($path);

        if ($result === false) {
            throw IOException::fromLastError();
        }

        return $result;
    }

    /**
     * Retrieves the file's creation date.
     *
     * @param string $path        the file to retrieve the creation date from
     * @param bool   $ignoreCache if the stat cache should be bypassed
     *
     * @throws IOException
     *
     * @return \DateTime the file's creation date
     */
    public static function getCreationDate(string $path, bool $ignoreCache = false): \DateTime
    {
        if (!is_file($path)) {
            throw new FileNotFoundException($path);
        }

        if ($ignoreCache) {
            clearstatcache(true, $path);
        }

        $time = @filectime($path);

        if ($time === false) {
            throw IOException::fromLastError();
        }

        return \DateTime::createFromFormat('U', (string) $time);
    }

    /**
     * Retrieves the file's modification date.
     *
     * @param string $path        the file to retrieve the modification date from
     * @param bool   $ignoreCache if the stat cache should be bypassed
     *
     * @throws IOException
     *
     * @return \DateTime the file's creation date
     */
    public static function getModificationDate(string $path, bool $ignoreCache = false): \DateTime
    {
        if (!is_file($path)) {
            throw new FileNotFoundException($path);
        }

        if ($ignoreCache) {
            clearstatcache(true, $path);
        }

        $time = @filemtime($path);

        if ($time === false) {
            throw IOException::fromLastError();
        }

        return \DateTime::createFromFormat('U', (string) $time);
    }

    /**
     * Retrieves the file's access date.
     *
     * @param string $path        the file to retrieve the access date from
     * @param bool   $ignoreCache if the stat cache should be bypassed
     *
     * @throws FileNotFoundException
     * @throws IOException
     *
     * @return \DateTime the file's access date
     */
    public static function getAccessDate(string $path, bool $ignoreCache = false): \DateTime
    {
        if (!is_file($path)) {
            throw new FileNotFoundException($path);
        }

        if ($ignoreCache) {
            clearstatcache(true, $path);
        }

        $time = @fileatime($path);

        if ($time === false) {
            throw IOException::fromLastError();
        }

        return \DateTime::createFromFormat('U', (string) $time);
    }

    /**
     * Sets the file's last access date.
     *
     * @param string    $path       the file
     * @param \DateTime $accessDate the file's access date
     *
     * @throws IOException
     */
    public static function setAccessDate(string $path, \DateTime $accessDate): void
    {
        if (!is_file($path)) {
            throw new FileNotFoundException($path);
        }

        clearstatcache(true, $path);

        $time = @filemtime($path);

        if ($time === false) {
            throw IOException::fromLastError();
        }

        if (!@touch($path, $time, $accessDate->getTimestamp())) {
            throw IOException::fromLastError();
        }
    }

    /**
     * Sets the file's last modification date.
     *
     * @param string    $path             the file
     * @param \DateTime $modificationDate the file's modification date
     *
     * @throws IOException
     */
    public static function setModificationDate(string $path, \DateTime $modificationDate): void
    {
        if (!is_file($path)) {
            throw new FileNotFoundException($path);
        }

        if (!@touch($path, $modificationDate->getTimestamp())) {
            throw IOException::fromLastError();
        }
    }

    /**
     * Tries to guess the provided file's type from its content.
     *
     * @param string $path the file's path
     *
     * @throws IOException
     *
     * @return FileType the file's type
     */
    public static function guessTypeFromContent(string $path): FileType
    {
        if (!is_file($path)) {
            throw new FileNotFoundException($path);
        }

        return self::reading($path, static function (StreamInterface $s): FileType {
            return FileTypeSnifferRegistry::getInstance()->guessFromContent($s);
        });
    }

    /**
     * Tries to guess the provided file's type from its name.
     *
     * @param string $name the file's name
     *
     * @return FileType the file's type
     */
    public static function guessTypeFromName(string $name): FileType
    {
        return FileTypeSnifferRegistry::getInstance()->guessFromName($name);
    }
}
