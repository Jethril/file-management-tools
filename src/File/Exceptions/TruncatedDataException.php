<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\File\Exceptions;

/**
 * Thrown when a file's data size does not match the requirements.
 */
class TruncatedDataException extends \RuntimeException
{
    public function __construct(string $file, int $actualSize, int $expectedSize)
    {
        parent::__construct("{$actualSize} were read from '{$file}' but {$expectedSize} were effectively read!");
    }
}
