<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\File\Exceptions;

/**
 * Thrown when the end of a stream is reached before the end of a reading sequence (e.g file is finished while reading
 * the second byte of a short integer).
 */
class EndOfStreamException extends IOException
{
    public function __construct()
    {
        parent::__construct('End of stream reached before all bytes were read!');
    }
}
