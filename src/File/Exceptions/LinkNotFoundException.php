<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\File\Exceptions;

/**
 * Thrown when a link does not exists.
 */
class LinkNotFoundException extends IOException
{
    public function __construct(string $link)
    {
        parent::__construct("Link '{$link}' does not exists!");
    }
}
