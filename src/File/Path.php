<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\File;

use FileManagementTools\OperatingSystem\OperatingSystem;
use InvalidArgumentException;

/**
 * Allows managing paths in a cross-platform way, without dealing with the real underlying file system.
 * This tool can be used to concatenate shell or FTP paths on different operating systems, for instance.
 */
final class Path
{
    /**
     * List of all possible directory separators.
     */
    private const SEPARATORS = '/\\';

    /**
     * List of all possible directory separators, in a regex.
     */
    private const SEPARATORS_REGEX = '#[/\\\\]+#';

    /**
     * Regex invalidating file names.
     */
    private const INVALID_FILE_NAME_REGEX = '/(?:^(?:\.\.?|(?:CON|PRN|AUX|NUL|COM[1-9]|LPT[1-9])\.?.*)$)|[\/\\\\<>:"|?*\x00-\x1F]|[. ]$/i';

    /**
     * Regex identifying root paths.
     */
    private const ROOT_PATH_REGEX = '#^(?:[a-z]:|/)[\\\\/]*$#i';

    private function __construct()
    {
    }

    /**
     * Retrieves the name of the file for the provided path.
     *
     * @param string $path the path to extract the file name from
     *
     * @return string the file name or `null` if the provided path is the root path
     */
    public static function getFileName(string $path): ?string
    {
        if (empty($path)) {
            throw new InvalidArgumentException('Path cannot be empty!');
        }

        if (self::isRoot($path)) {
            return null;
        }

        $segments    = self::getSegments($path);
        $lastSegment = $segments[\count($segments) - 1];

        // If not a valid file name the path must be a ., a .. or must be invalid.
        if (!self::isValidFileName($lastSegment)) {
            throw new InvalidArgumentException('The provided path is invalid or points to a relative location!');
        }

        return $lastSegment;
    }

    /**
     * Retrieves the name of the file, without its extension.
     *
     * @param string $path the path to extract the file name from
     *
     * @return null|string the file name, or `null` if the given path is a root path
     */
    public static function getFileNameWithoutExtension(string $path): ?string
    {
        $fileName = self::getFileName($path);

        if ($fileName === null) {
            return null;
        }

        $lastDotPos = strrpos($fileName, '.');

        return $lastDotPos === 0 || $lastDotPos === false ? $fileName : substr($fileName, 0, $lastDotPos);
    }

    /**
     * Retrieves the extension from the provided path.
     *
     * @param string $path the path to extract the file name from
     *
     * @return null|string the extension, or `null` if no extension was identified
     */
    public static function getExtension(string $path): ?string
    {
        $fileName = self::getFileName($path);

        if ($fileName === null) {
            return null;
        }

        $lastDotPos = strrpos($fileName, '.');

        return $lastDotPos === 0 || $lastDotPos === false || $lastDotPos === \strlen($path) - 1
            ? null
            : strtolower(substr($fileName, $lastDotPos + 1));
    }

    /**
     * Split the paths into segments.
     *
     * @param string $path the path to split
     *
     * @return array the segments
     */
    public static function getSegments(string $path): array
    {
        if (empty($path)) {
            throw new InvalidArgumentException('Path cannot be empty!');
        }

        $segments = array_filter(preg_split(self::SEPARATORS_REGEX, $path));
        $first    = $path[0] ?? null;

        return array_values(
            $first !== null && strpos(self::SEPARATORS, $first) !== false
                ? array_merge([$first], $segments)
                : $segments
        );
    }

    /**
     * Determines if the provided path is root.
     *
     * @param string $path the path to test
     *
     * @return bool `true` if the path is root, `false` otherwise
     */
    public static function isRoot(string $path): bool
    {
        if (empty($path)) {
            throw new InvalidArgumentException('Path cannot be empty!');
        }

        return (bool) preg_match(self::ROOT_PATH_REGEX, $path);
    }

    /**
     * Retrieves the parent segment of the provided path.
     *
     * @param string $path The path
     *
     * @return null|string the path's parent segment path, or `null` if the path has no parent
     */
    public static function getParentSegment(string $path): ?string
    {
        if (empty($path)) {
            throw new InvalidArgumentException('Path cannot be empty!');
        }

        if (self::isRoot($path)) {
            return $path;
        }

        $segments = self::getSegments($path);

        if (\count($segments) === 1) {
            return null;
        }

        return $segments[\count($segments) - 2];
    }

    /**
     * Merges several paths into one with the current system's separator.
     *
     * @param string ...$parts the parts of the path
     *
     * @return string the joined path
     */
    public static function join(string ...$parts): string
    {
        return self::joinWithOs(OperatingSystem::getCurrent(), ...$parts);
    }

    /**
     * Merges several paths into one using a custom separator.
     *
     * @param int    $operatingSystem the operating system
     * @param string ...$parts        the parts of the path
     *
     * @return string the joined path
     */
    public static function joinWithOs(int $operatingSystem, string ...$parts): string
    {
        $separator        = $operatingSystem === OperatingSystem::WINDOWS ? '\\' : '/';
        $leadingSeparator = strpos(self::SEPARATORS, $parts[0][0] ?? ' ') === false ? '' : $separator;
        $result           = [];

        foreach ($parts as $part) {
            $part = preg_replace(self::SEPARATORS_REGEX, $separator, trim($part, self::SEPARATORS));

            if (!empty($part)) {
                $result[] = $part;
            }
        }

        return $leadingSeparator . implode($separator, $result);
    }

    /**
     * Makes the provided path absolute based on the supplied current path and joins it using the provided separator.
     *
     * Example:
     * ```php
     * echo Path::makeAbsolute('./users.txt', '/home/user'); // Outputs /home/user/users.txt
     * echo Path::makeAbsolute('./../users.txt', '/home'); // Outputs /users.txt
     * ```
     *
     * @param string   $path            the path to make absolute
     * @param string   $current         the current path, absolute
     * @param null|int $operatingSystem the OS, or `null` to use the current one
     *
     * @return string the absolute path
     */
    public static function makeAbsolute(string $path, string $current, ?int $operatingSystem = null): string
    {
        if (empty($path)) {
            throw new InvalidArgumentException('Path cannot be empty!');
        }

        if (!self::isAbsolute($current)) {
            throw new InvalidArgumentException('Current path must be absolute!');
        }

        if (self::isAbsolute($path)) {
            return $path;
        }

        if ($operatingSystem === null) {
            $operatingSystem = OperatingSystem::getCurrent();
        }

        $currentSegments = self::getSegments($current);

        if ($operatingSystem === OperatingSystem::WINDOWS && ($path[0] ?? '') === '\\') {
            $result = [];
        } else {
            $result = \array_slice($currentSegments, 1);
        }

        foreach (preg_split(self::SEPARATORS_REGEX, $path) as $segment) {
            if ('..' === $segment) {
                array_pop($result);
            } elseif ('.' !== $segment) {
                $result[] = $segment;
            }
        }

        return self::joinWithOs($operatingSystem, ...array_merge([$currentSegments[0]], $result));
    }

    /**
     * Makes the provided path relative based on the supplied current path and joins it using the provided separator.
     *
     * Example:
     * ```php
     * echo Path::makeRelative('/home/user/users.txt', '/home/user'); // Outputs ./users.txt
     * echo Path::makeRelative('/tmp/users.txt', '/home/user'); // Outputs ../../tmp/users.txt
     * ```
     *
     * @param string   $path            the path to make relative
     * @param string   $current         the current path, absolute
     * @param null|int $operatingSystem the OS, or `null` to use the current one
     *
     * @return string the relative path
     */
    public static function makeRelative(string $path, string $current, ?int $operatingSystem = null): string
    {
        if (empty($path)) {
            throw new InvalidArgumentException('Path cannot be empty!');
        }

        if (!self::isAbsolute($current)) {
            throw new InvalidArgumentException('Current path must be absolute!');
        }

        if ($operatingSystem === null) {
            $operatingSystem = OperatingSystem::getCurrent();
        }

        // If already relative, make it absolute to clean up dots that aren't required.
        if (!self::isAbsolute($path)) {
            $path = self::makeAbsolute($path, $current, $operatingSystem);
        }

        $currentSegments = self::getSegments($current);
        $pathSegments    = self::getSegments($path);

        if ($currentSegments[0] !== $pathSegments[0]) {
            throw new InvalidArgumentException('Cannot make relative paths if roots are different!');
        }

        $result    = [];
        $different = -1;
        $max       = max(\count($currentSegments), \count($pathSegments));

        for ($i = 0, $j = 0; $i < $max || $j < $max; ++$i, ++$j) {
            if (isset($pathSegments[$i]) && !isset($currentSegments[$j])) {
                // If path is longer than the current path, add the path elements to the result.
                $result[] = $pathSegments[$i];
            } elseif (!isset($pathSegments[$i]) && isset($currentSegments[$j])) {
                // If current path is longer than path, add .. element to the result.
                $result[] = '..';
            } elseif (isset($pathSegments[$i], $currentSegments[$j]) &&
                ($different !== -1 || $pathSegments[$i] !== $currentSegments[$j])) {
                // This part helps solving cases where /home/users is made relative to /home/other/dir (where the
                // algorithm must ignore the first /home but keep ".." for each segment after the last common segment.
                // Should result in ../../users, although adding /home as .. will be semantically valid.

                if ($different === -1) {
                    $different = $i - 1;
                }

                $result[] = '..';
                $i        = $different;
            }
        }

        return empty($result) ? '.' : self::joinWithOs($operatingSystem, ...$result);
    }

    /**
     * Determines if two paths are equal, depending on the platform.
     *
     * **Note**: a path is considered equal to another if segments are equal. Does not test real references.
     *
     * @param string   $path1           the first path
     * @param string   $path2           the second path
     * @param null|int $operatingSystem the OS, or `null` to use the current OS
     *
     * @return bool `true` if the paths are equal, `false` otherwise
     */
    public static function areEqual(string $path1, string $path2, ?int $operatingSystem = null): bool
    {
        if ($operatingSystem === null) {
            $operatingSystem = OperatingSystem::getCurrent();
        }

        $windows   = $operatingSystem === OperatingSystem::WINDOWS;
        $separator = $windows ? '\\' : '/';
        $path1     = rtrim(preg_replace(self::SEPARATORS_REGEX, $separator, $path1), $separator);
        $path2     = rtrim(preg_replace(self::SEPARATORS_REGEX, $separator, $path2), $separator);

        return $path1 === $path2 || ($windows && strtolower($path1) === strtolower($path2));
    }

    /**
     * Determines if the provided path is contained inside another one.
     *
     * @param string   $path            the path to test against the prefix
     * @param string   $prefix          the prefix, must be absolute
     * @param null|int $operatingSystem the OS, or `null` to use the current OS
     *
     * @return bool `true` if the path starts with `$prefix`, `false` otherwise
     */
    public static function startsWith(string $path, string $prefix, ?int $operatingSystem = null): bool
    {
        if (empty($path)) {
            throw new InvalidArgumentException('Path cannot be empty!');
        }

        if (empty($prefix)) {
            throw new InvalidArgumentException('Prefix cannot be empty!');
        }

        if ($path === $prefix) {
            return true;
        }

        $windows        = ($operatingSystem ?? OperatingSystem::getCurrent()) === OperatingSystem::WINDOWS;
        $pathSegments   = self::getSegments($path);
        $prefixSegments = self::getSegments($prefix);
        $prefixSize     = \count($prefixSegments);

        if (\count($pathSegments) < $prefixSize) {
            return false;
        }

        for ($i = 0; $i < $prefixSize; ++$i) {
            if ($pathSegments[$i] !== $prefixSegments[$i] &&
                (!$windows || strtolower($pathSegments[$i]) !== strtolower($prefixSegments[$i]))) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determines if a path is absolute.
     *
     * @param string $path the path to test
     *
     * @return bool `true` if the path is absolute, `false` otherwise
     */
    public static function isAbsolute(string $path): bool
    {
        if (empty($path)) {
            throw new InvalidArgumentException('Path cannot be empty!');
        }

        return self::isRoot(self::getSegments($path)[0]);
    }

    /**
     * Checks whether the provided path is valid or not.
     *
     * @param string $path the path to check
     *
     * @return bool `true` if the path is valid, `false` otherwise
     */
    public static function isValidPath(string $path): bool
    {
        if (empty($path)) {
            return false;
        }

        $segments = self::getSegments($path);

        if (!empty($segments) && $segments[0] === '/') {
            unset($segments[0]);
        }

        foreach ($segments as $segment) {
            if (!self::isValidSegment($segment)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks whether a path segment is valid or not.
     *
     * @param string $segment the segment to validate
     *
     * @return bool true if the segment is valid, false otherwise
     */
    public static function isValidSegment(string $segment): bool
    {
        return '.' === $segment || '..' === $segment || self::isValidFileName($segment);
    }

    /**
     * Checks whether the provided file name is valid or not.
     *
     * @param string $name the name to check
     *
     * @return bool true if the file is valid, false otherwise
     */
    public static function isValidFileName(string $name): bool
    {
        return !empty($name) && !preg_match(self::INVALID_FILE_NAME_REGEX, $name);
    }

    /**
     * Removes all trailing separators from the provided string.
     *
     * @param string $item the string to remove the separators from
     *
     * @return string the string without the trailing separators
     */
    public static function removeTrailingSeparators(string $item): string
    {
        return rtrim($item, self::SEPARATORS);
    }

    /**
     * Removes all leading separators from the provided string.
     *
     * @param string $item the string to remove the separators from
     *
     * @return string the string without the leading separators
     */
    public static function removeLeadingSeparators(string $item): string
    {
        return ltrim($item, self::SEPARATORS);
    }
}
