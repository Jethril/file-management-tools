<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\File;

use FileManagementTools\File\Exceptions\ElementAlreadyExistsException;
use FileManagementTools\File\Exceptions\IOException;
use FileManagementTools\File\Exceptions\LinkNotFoundException;

/**
 * Allows managing symbolic links.
 */
final class Link
{
    private function __construct()
    {
    }

    /**
     * Tests if the provided link exists on the disk.
     *
     * @param string $path        the link's path
     * @param bool   $ignoreCache `true` if the PHP cache should be ignored, `false` otherwise
     *
     * @return bool `true` if the link exists, `false` otherwise
     */
    public static function exists(string $path, bool $ignoreCache = false): bool
    {
        if ($ignoreCache) {
            clearstatcache(true, $path);
        }

        return is_link($path);
    }

    /**
     * Creates a symbolic link between two filesystem entries.
     *
     * Note: on Windows, PHP must be executed with elevated rights!
     *
     * @param string $path   the link's path
     * @param string $target the link's target path (must be absolute!)
     *
     * @throws IOException
     */
    public static function create(string $path, string $target): void
    {
        if (!Path::isAbsolute($target)) {
            throw new \InvalidArgumentException('The provided target must be absolute!');
        }

        if (!is_file($target) && !is_dir($target) && !is_link($target)) {
            throw new \InvalidArgumentException('The provided target does not exists!');
        }

        if (is_link($path)) {
            throw new ElementAlreadyExistsException("Link '{$path}' already exists!");
        }

        if (is_file($path)) {
            throw new ElementAlreadyExistsException("Path '{$path}' exists and is a file!");
        }

        if (is_dir($path)) {
            throw new ElementAlreadyExistsException("Path '{$path}' exists and is a directory!");
        }

        if (!@symlink($target, $path)) {
            throw IOException::fromLastError();
        }
    }

    /**
     * Deletes the provided link.
     *
     * Note: on Windows, PHP must be executed with elevated rights!
     *
     * @param string $path              the link's path
     * @param bool   $ignoreNonExisting if non-existing links should be ignored (`true`) or make this method fail
     *                                  (`false`)
     *
     * @throws IOException
     */
    public static function delete(string $path, bool $ignoreNonExisting = false): void
    {
        if (!is_link($path)) {
            if ($ignoreNonExisting) {
                return;
            }

            throw new LinkNotFoundException($path);
        }

        if (!@unlink($path) && !@rmdir($path)) {
            throw IOException::fromLastError();
        }
    }

    /**
     * Retrieves the provided link's target.
     *
     * @param string $path the link to retrieve the target from
     *
     * @throws IOException
     *
     * @return string the path of the link's target
     */
    public static function getTarget(string $path): string
    {
        if (!is_link($path)) {
            throw new LinkNotFoundException($path);
        }

        $target = @readlink($path);

        if ($target === false) {
            throw IOException::fromLastError();
        }

        return $target;
    }
}
