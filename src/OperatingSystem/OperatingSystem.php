<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\OperatingSystem;

/**
 * Enum of known operating systems.
 */
final class OperatingSystem
{
    /**
     * An unknown operating system.
     */
    public const UNKNOWN = 0;

    /**
     * The Linux operating system.
     */
    public const LINUX = 1;

    /**
     * The Windows operating system.
     */
    public const WINDOWS = 2;

    /**
     * The MacOS operating system.
     */
    public const MAC_OS = 3;

    /**
     * The Solaris operating system.
     */
    public const SOLARIS = 4;

    /**
     * @var null|int cache for the current OS
     */
    private static $CURRENT;

    private function __construct()
    {
    }

    /**
     * @return int the current operating system
     */
    public static function getCurrent(): int
    {
        if (self::$CURRENT === null) {
            if (stripos(PHP_OS, 'DAR') !== false) {
                self::$CURRENT = self::MAC_OS;
            } elseif (stripos(PHP_OS, 'WIN') !== false) {
                self::$CURRENT = self::WINDOWS;
            } elseif (stripos(PHP_OS, 'LINUX') !== false) {
                self::$CURRENT = self::LINUX;
            } elseif (stripos(PHP_OS, 'SUN') !== false) {
                self::$CURRENT = self::SOLARIS;
            } else {
                self::$CURRENT = self::UNKNOWN;
            }
        }

        return self::$CURRENT;
    }
}
